//
//  User+CoreDataProperties.m
//  
//
//  Created by Nikita Ilyukhin on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

@implementation User

@dynamic nameUser;
@dynamic emailUser;
@dynamic photoURL;
@dynamic photoLocal;
@dynamic accessTokenVK;
@dynamic accessTokenFB;

@end
