//
//  ChangePasswordView.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/27/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "ChangePasswordView.h"

@implementation ChangePasswordView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(BOOL)verificationField
{
    if (self.textFieldNewPassword.text.length<6 || self.textFieldOldPassword.text.length<6)
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Пароль должен содержать не менее 6 символов!"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
         {
         }];
        return NO;
    }
    else if (self.textFieldOldPassword.text.length>50 || self.textFieldNewPassword.text.length>50)
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Пароль не должен содержать более 50 символов!"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
         {
         }];
        return NO;
    }
    
    else if ([self.textFieldOldPassword.text isEqualToString: self.textFieldNewPassword.text])
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Пароли не должны совпадать!"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
         {
         }];
        return NO;
    }
    return YES;
}

- (IBAction)pressedButtonChangePassword:(id)sender
{
    if ([self verificationField])
    {
         [self.delegate hideChangePasswordView];
    }
}
@end
