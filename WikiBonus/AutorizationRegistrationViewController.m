//
//  AutorizationRegistrationViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/30/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "AutorizationRegistrationViewController.h"
#import "AutorizationViewController.h"
#import "RegistrationViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
@interface AutorizationRegistrationViewController () <UIContentContainer, AutorizationViewControllerDelegate>
{
    AutorizationViewController* _authVC;
    RegistrationViewController* _regVC;
    IBOutlet UIView *contentView;
    IBOutlet UIImageView *imageViewLogo;
}

@end

@implementation AutorizationRegistrationViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_authVC == nil)
    {
        _authVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AutorizationViewController"];
        [self addChildViewController:_authVC];
        _authVC.delegate = self;
        [contentView addSubview:_authVC.view];
        _authVC.view.hidden = NO;
    }
    if (_regVC == nil)
    {
        _regVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
        [self addChildViewController:_regVC];
        [contentView addSubview:_regVC.view];
        _regVC.view.hidden = YES;
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.containtViewHeight.constant = 284;  
    [self.segmentControlAutorization setSelectedSegmentIndex:0];
    [self.segmentControlAutorization setTitle:DPLocalizedString(@"authorization", nil) forSegmentAtIndex:0];
    [self.segmentControlAutorization setTitle:DPLocalizedString(@"registration", nil) forSegmentAtIndex:1];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(244, 110), NO, scaleScreen);
    [WikiBonusStyleKit drawLogoProjectIconWithFrame:CGRectMake(0, 0, 244, 110)];
    [imageViewLogo setImage:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)segmentControllValueChanged:(id)sender
{
    if(self.segmentControlAutorization.selectedSegmentIndex == 1)
    {
        _regVC.view.hidden = NO;
        _authVC.view.hidden = YES;
        self.containtViewHeight.constant = 500;
        [self.view setNeedsDisplay];
    }
    else if(self.segmentControlAutorization.selectedSegmentIndex == 0)
    {
        _regVC.view.hidden = YES;
        _authVC.view.hidden = NO;
        self.containtViewHeight.constant = 284;
        [self.view setNeedsDisplay];
    }
}

#pragma mark - AutorizationViewControllerDelegate
-(void)showRegViewControllerWithParamName:(NSString *)name gender:(NSString *)gender
{
    _regVC.textFieldName.text = name;
    _regVC.sex = gender;
    [self.segmentControlAutorization setSelectedSegmentIndex:1];
    [self.segmentControlAutorization sendActionsForControlEvents:UIControlEventValueChanged];
    [VKSdk forceLogout];
    [[FBSDKLoginManager new] logOut];
}

-(void)showProgressHUD
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgressHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
