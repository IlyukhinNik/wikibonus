//
//  popUpView.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/19/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PoinOfSale.h"
#import "MKAnnotationCustom.h"

@protocol PopUpViewDelegate <NSObject>
@required

- (void)showPointOfSaleViewControllerWithPointOfSale:(PointOfSale*)pointOfSale;

@end

@interface PopUpView : UIView

@property (weak, nonatomic) id<PopUpViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRate;
@property (weak, nonatomic) IBOutlet UIButton *buttonRestoraunt;
@property (nonatomic) float starRate;
@property (nonatomic) PointOfSale *pointOfSale;

-(void)updatePopUpViewDataWithPinAnnotation:(MKAnnotationCustom*)annotationCustom;
- (IBAction)buttonDetailInfoPressed:(id)sender;

@end
