//
//  MyDesiresTableViewCell.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGiftTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDistanceToUser;
@property (weak, nonatomic) IBOutlet UILabel *labelAdressPartner;
@property (weak, nonatomic) IBOutlet UILabel *labelNamePartner;
@property (weak, nonatomic) IBOutlet UILabel *labelNameGift;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhotoGift;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProcentGift;
@property (weak, nonatomic) IBOutlet UILabel *labelProcentGift;
@property (weak, nonatomic) IBOutlet UILabel *labelExpiredDate;

@end
