//
//  GPManager.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 3/1/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "GPManager.h"

@implementation GPManager


- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    // ...
}

@end
