//
//  FriendsNewsViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "FriendsNewsViewController.h"

@interface FriendsNewsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableViewFriends;

@end

@implementation FriendsNewsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    
//    return _friendsArray.count;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 118;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    MyGiftTableViewCell* cell = [self.tableViewMyGifts dequeueReusableCellWithIdentifier:@"cellGifts"];
//    if (cell == nil) {
//        
//        cell = [[MyGiftTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellGifts"];
//    }
//    cell.labelNameGift.text = [_giftsArray objectAtIndex:indexPath.row];
//    return cell;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
