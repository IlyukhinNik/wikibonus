//
//  GPManager.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 3/1/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Google/SignIn.h>

@interface GPManager : NSObject  <GIDSignInDelegate>

-(void)logIn;
-(void)logOut;

@end
