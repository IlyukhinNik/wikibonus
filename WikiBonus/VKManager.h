//
//  VKManager.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 3/1/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKSdk.h"
#import <AFNetworking/AFNetworking.h>
#import <UIAlertView+Block.h>


@interface VKManager : NSObject <VKSdkDelegate, VKSdkUIDelegate>

@property (nonatomic) VKAccessToken* vkAccessToken;

-(BOOL)logIn;
-(void)logOut;
-(NSDictionary*)getUserData;
-(VKUser*)getVKUser;
-(VKUsersArray*)getFriendsArray;
-(void)shareVKText:(NSString*)text andImagesArray:(NSArray*)imagesArray andUrl:(NSURL*)url andVC:(UIViewController*)vc;


@end
