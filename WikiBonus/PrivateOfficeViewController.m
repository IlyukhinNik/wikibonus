//
//  PrivateOfficeViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "PrivateOfficeViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "MyGiftsViewController.h"
#import "Gift.h"
#import "Fragmen.h"

@interface PrivateOfficeViewController () <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
    NSArray *_propertyArray;
    NSMutableArray *_testGiftsArray;
    NSMutableArray *_testGiftsInDesires;
    NSMutableArray *_testFragmentArray;
    
}
@property (weak, nonatomic) IBOutlet UILabel *labelGiftCount;
@property (weak, nonatomic) IBOutlet UILabel *labekFragmentsCount;
@property (weak, nonatomic) IBOutlet UITableView *tableViewProperties;
@end

@implementation PrivateOfficeViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    [_testGiftsInDesires removeAllObjects];
    for ( Gift *tmpGift in _testGiftsArray)
    {
        if (tmpGift.inWishes)
        {
            [_testGiftsInDesires addObject:tmpGift];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTestFragments];
    [self setTestGifts];
    self.title = DPLocalizedString(@"cabinet", nil);
    self.labelGiftCount.text = [NSString stringWithFormat:@"%lu", (unsigned long)_testGiftsArray.count];
    self.labekFragmentsCount.text = [NSString stringWithFormat:@"%lu", (unsigned long)_testFragmentArray.count];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    _propertyArray = @[DPLocalizedString(@"friendsNews", nil), DPLocalizedString(@"myGifts", nil), DPLocalizedString(@"myDesires", nil), DPLocalizedString(@"profile", nil), DPLocalizedString(@"inviteFriend", nil), DPLocalizedString(@"hallOfFame", nil), DPLocalizedString(@"aboutProject", nil)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setTestGifts
{
    _testGiftsArray = [NSMutableArray new];
    _testGiftsInDesires = [NSMutableArray new];
    Gift *testGift1= [Gift new];
    [testGift1 updateGiftWithIDGift:1 nameGift:@"Тортик в подарок" description:@"Соберите 3 фрагмента и получите в подарок бесплатный тортик от кондитерской Карамель" image:[UIImage imageNamed:@"gift.png"] likesCount:12 reviewsCount:3 idPointOfSale:1 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:NO procentFull:0.75];
    [_testGiftsArray addObject:testGift1];
    Gift *testGift2= [Gift new];
    [testGift2 updateGiftWithIDGift:2 nameGift:@"Бокал пива в подарок" description:@"Соберите все фрагменты и получите бесплатный бокал пива" image:[UIImage imageNamed:@"gift.png"] likesCount:4 reviewsCount:1 idPointOfSale:4 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:YES isRecieved:YES procentFull: 1.0];
    [_testGiftsArray addObject:testGift2];
    Gift *testGift3= [Gift new];
    [testGift3 updateGiftWithIDGift:3 nameGift:@"Кофе в подарок" description:@"Соберите все фрагменты и получите кофе в подарок" image:[UIImage imageNamed:@"gift.png"] likesCount:4 reviewsCount:1 idPointOfSale:3 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:NO procentFull: 0.0];
    [_testGiftsArray addObject:testGift3];
}

-(void)setTestFragments
{
    _testFragmentArray = [NSMutableArray new];
    Fragmen* testFragment1 = [Fragmen new];
    [testFragment1 updateFragmentWithID:1 nameFragment:@"First" imageFragment:[UIImage imageNamed:@"fragment1.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment1];
    Fragmen* testFragment2 = [Fragmen new];
    [testFragment2 updateFragmentWithID:1 nameFragment:@"Second" imageFragment:[UIImage imageNamed:@"fragment2.png"] dateDead:[NSDate date] isReceived:NO];
    [_testFragmentArray addObject:testFragment2];
    Fragmen* testFragment3 = [Fragmen new];
    [testFragment3 updateFragmentWithID:1 nameFragment:@"Third" imageFragment:[UIImage imageNamed:@"fragment3.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment3];
    Fragmen* testFragment4 = [Fragmen new];
    [testFragment4 updateFragmentWithID:1 nameFragment:@"Fourth" imageFragment:[UIImage imageNamed:@"fragment4.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment4];
    Fragmen* testFragment5 = [Fragmen new];
    [testFragment4 updateFragmentWithID:1 nameFragment:@"5" imageFragment:[UIImage imageNamed:@"fragment4.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment5];
    Fragmen* testFragment6 = [Fragmen new];
    [testFragment6 updateFragmentWithID:1 nameFragment:@"6" imageFragment:[UIImage imageNamed:@"fragment4.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment4];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _propertyArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    UITableViewCell* cell = [self.tableViewProperties dequeueReusableCellWithIdentifier:WB_PROPERTY_CELL];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:WB_PROPERTY_CELL];
    }
    cell.textLabel.text = [_propertyArray objectAtIndex:indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
            [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"inDevelopment", nil) message:nil
                              cancelButtonTitle:DPLocalizedString(@"close", nil)]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 1)
                 {
                     // delete
                 }
             }];
            break;
        case 1:
            [self performSegueWithIdentifier:@"segueMyGifts" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"segueMyDesires" sender:self];
            break;
        case 3:
            [self performSegueWithIdentifier:@"segueUserProfile" sender:self];
            
            break;
            
        case 4:
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                mail.mailComposeDelegate = self;
                [mail setSubject:@"Присоединяйся со мной к WikiBonus!"];
                [mail setMessageBody:@"Какой-то мега-крутой рекламный текст с приглашением! ну просто супер текст! никто не откажется!" isHTML:NO];
                [mail setToRecipients:nil];
                
                [self presentViewController:mail animated:YES completion:NULL];
            }
            else
            {
                NSLog(@"This device cannot send email");
            }
            break;
        case 5:
            [self performSegueWithIdentifier:@"segueHallOfFame" sender:self];
            break;
        case 6:
           [self performSegueWithIdentifier:@"segueAboutProject" sender:self];
            break;
        default:

            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueMyGifts"])
    {
        MyGiftsViewController *myGiftsView = [segue destinationViewController];
        myGiftsView.title = DPLocalizedString(@"myGifts", nil);
        myGiftsView.giftsArray = _testGiftsArray;
    }
    if ([[segue identifier] isEqualToString:@"segueMyDesires"])
    {
        MyGiftsViewController *myDesiresView = [segue destinationViewController];
        myDesiresView.title = DPLocalizedString(@"myDesires", nil);
        myDesiresView.giftsArray = _testGiftsInDesires;        
    }
}

@end
