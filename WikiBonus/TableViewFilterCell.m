//
//  TableViewFilterCell.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/24/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "TableViewFilterCell.h"

@implementation TableViewFilterCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
