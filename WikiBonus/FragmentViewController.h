//
//  FragmentViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/25/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Fragmen.h"

@interface FragmentViewController : UIViewController

@property (strong, nonatomic) Fragmen *currentFragment;

@end
