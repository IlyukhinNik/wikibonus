//
//  ProfileViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/9/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "ProfileViewController.h"
#import "WBParser.h"
#import "User.h"
#import "WBAPIClient.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "KLCPopup.h"
#import <QuartzCore/QuartzCore.h>
#import "ChangePasswordView.h"

@interface ProfileViewController () <ChangePasswordViewDelegate, UITextFieldDelegate>
{
    NSArray* _propertyArray;
    User* _localUser;
    KLCPopup *_popupChangePassword;
    NSUserDefaults *_userDefaults;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageViewEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhone;


@property (weak, nonatomic) IBOutlet UILabel *labelProcentFull;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCity;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;

//@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
//@property (weak, nonatomic) IBOutlet UILabel *labelPhoneNumber;
//@property (weak, nonatomic) IBOutlet UILabel *labelFirstName;
//@property (weak, nonatomic) IBOutlet UILabel *labelLastName;
@property (weak, nonatomic) IBOutlet UILabel *labelGender;
@property (weak, nonatomic) IBOutlet UIProgressView *progressViewIsFilledProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhotoUser;
@property (weak, nonatomic) IBOutlet UITableView *tableViewProperty;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) NSDateFormatter *dateFormat;
@end
@implementation ProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = DPLocalizedString(@"profile", nil);
    self.imageViewPhotoUser.layer.cornerRadius = self.imageViewPhotoUser.frame.size.height /2;;
    self.imageViewPhotoUser.layer.masksToBounds = YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    _propertyArray = @[@"Изменить пароль", @"Удалить аккаунт"];
    datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setDate:[NSDate date]];
    [datePicker setMaximumDate:[NSDate date]];
    
    [self.dateTextField setInputView:datePicker];
    self.dateTextField.userInteractionEnabled = NO;
    
    UIToolbar *toolBar = [UIToolbar new];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *buttonDone =  [[UIBarButtonItem alloc] initWithTitle:@"Готово" style: UIBarButtonItemStyleDone target:self action:@selector(updateTextField)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, buttonDone, nil]];
    [self.dateTextField setInputAccessoryView:toolBar];
    self.dateTextField.inputAccessoryView.frame = CGRectMake(0, 0, 320, 50);
    float _procentFull = 0.32;
    self.progressViewIsFilledProfile.progress = _procentFull;
    self.labelProcentFull.text = [NSString stringWithFormat:@"%d%%", (int)ceilf(_procentFull*100)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _localUser = [WBParser getUserFromPlistDictionary:[self loadUserDataFromPList]];
    if (_localUser.photoLocalURL.length > 0)
    {
        [self.imageViewPhotoUser setImage:[self loadImagewithName:@"UserInfo.png"]];
    }
    else
    {
        UIGraphicsBeginImageContextWithOptions(self.imageViewPhotoUser.bounds.size, NO, scaleScreen);
        [WikiBonusStyleKit drawNoImageIconWithFrame:self.imageViewPhotoUser.bounds];
        [self.imageViewPhotoUser setImage:UIGraphicsGetImageFromCurrentImageContext()];
        UIGraphicsEndImageContext();
    }
    self.textFieldFirstName.text = _localUser.firstNameUser;
    self.textFieldLastName.text = _localUser.lastNameUser;
    self.dateTextField.text = _localUser.bdayUser;
    self.textFieldEmail.text = _localUser.emailUser;
    self.textFieldPhoneNumber.text = _localUser.phoneUser;
    self.textFieldCity.text = _localUser.cityUser;
    if ([_localUser.gender isEqualToString:@"male"] )
    {
        self.labelGender.text = [NSString stringWithFormat:@" %@ %@", DPLocalizedString(@"sex", nil), DPLocalizedString(@"man", nil)];
    }
    else
    {
        self.labelGender.text = [NSString stringWithFormat:@" %@ %@", DPLocalizedString(@"sex", nil), DPLocalizedString(@"women", nil)];
    }
    UIGraphicsBeginImageContextWithOptions(self.imageViewEmail.bounds.size, NO, scaleScreen);
    [WikiBonusStyleKit drawInfoIconWithFrame:self.imageViewEmail.bounds];
    [self.imageViewEmail setImage:UIGraphicsGetImageFromCurrentImageContext()];
    [self.imageViewPhone setImage:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (NSDictionary*)loadUserDataFromPList
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths firstObject];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path])
    {
        NSLog(@"path doesn't exist. plist file will be copied to the path.");
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"UserInfo" ofType:@"plist"];
        if (bundlePath)
        {
            NSLog(@"file exists in the main bundle.");
            NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:bundlePath];
            NSLog(@"Bundle UserInfo.plist, %@", [resultDictionary description]);
            [fileManager copyItemAtPath:bundlePath toPath:path error:nil];
            NSLog(@"plist file is copied from main bundle to document directory");
        }
        else
        {
            NSLog(@"UserInfo.plist not found in main bundle.");
        }
        
    }
    NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    NSLog(@"Loaded UserInfo.plist, %@", [resultDictionary description]);
    
    return resultDictionary;
}

#pragma mark - Load local photo methods

- (UIImage*)loadImagewithName:(NSString*)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithString:name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

-(void)updateTextField
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    [self.dateTextField endEditing:YES];
    self.dateTextField.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePicker.date]];
    [self.dateTextField resignFirstResponder];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
    NSMutableDictionary *resultDictionary = [NSMutableDictionary dictionaryWithDictionary:[NSDictionary dictionaryWithContentsOfFile:path]];
#warning refactor validate
    [resultDictionary setObject:self.dateTextField.text forKey:@"birthday"];
    [resultDictionary writeToFile:path atomically:YES];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _propertyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [self.tableViewProperty dequeueReusableCellWithIdentifier:WB_PROPERTY_CELL];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:WB_PROPERTY_CELL];
    }
    cell.textLabel.text = [_propertyArray objectAtIndex:indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            UIAlertView *disclaimerAgreedAlertView = [[UIAlertView alloc] initWithTitle:@"Изменение пароля"
                                                                          message:nil
                                                                          delegate:self
                                                                          cancelButtonTitle:DPLocalizedString(@"cancel", nil)
                                                                          otherButtonTitles:DPLocalizedString(@"change", nil), nil];
            
            [disclaimerAgreedAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
            [[disclaimerAgreedAlertView textFieldAtIndex:0] setSecureTextEntry:YES];
            [disclaimerAgreedAlertView textFieldAtIndex:0].placeholder = DPLocalizedString(@"oldPassword", nil);
            [disclaimerAgreedAlertView textFieldAtIndex:1].placeholder = DPLocalizedString(@"newPassword", nil);
            [disclaimerAgreedAlertView showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 0)
                {
                     [tableView deselectRowAtIndexPath:indexPath animated:YES];
                     [self.view resignFirstResponder];
                }
                else if ( buttonIndex == 1)
                {
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                    [self.view resignFirstResponder];
                    _userDefaults = [NSUserDefaults standardUserDefaults];
                    [WBAPIClient changePasswordWithOldPassword:[disclaimerAgreedAlertView textFieldAtIndex:0].text newPassword:[disclaimerAgreedAlertView textFieldAtIndex:1].text complete:^(id responseObject, NSError *error)
                     {
                         if (error)
                         {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                             });
                             NSLog(@"%@", [error.userInfo objectForKey:@"message"]);
                             [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error", nil) message:DPLocalizedString(@"repeatOperationLater", nil)
                                               cancelButtonTitle:DPLocalizedString(@"close", nil)]
                              showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 1)
                                  {
                                      // delete
                                  }
                              }];
                         }
                         else
                         {
                             User* localUser = [WBParser getUser:responseObject andEmail:self.textFieldEmail.text];
                             NSLog(@"%@", localUser.userSID);
                             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                             [userDefaults setObject:[responseObject objectForKey:@"SID"] forKey:@"SID"];
                             [userDefaults synchronize];
                             [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"done", nil) message:DPLocalizedString(@"passwordSuccessfullyChanged", nil)
                                               cancelButtonTitle:DPLocalizedString(@"close", nil)]
                              showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 1)
                                  {
                                      // delete
                                  }
                              }];
                        }
                     }];
                }
            }];
        }
            break;
        case 1:
        {
            [[[UIAlertView alloc]   initWithTitle:DPLocalizedString(@"attention", nil)
                                          message:DPLocalizedString(@"doYouWantDeleteAcc", nil)
                                cancelButtonTitle:DPLocalizedString(@"cancel", nil)
                                otherButtonTitles:@[DPLocalizedString(@"deleteAccount", nil)]]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 0)
                 {
                  [tableView deselectRowAtIndexPath:indexPath animated:YES];
                 }
                 else if ( buttonIndex == 1)
                 {
                   [tableView deselectRowAtIndexPath:indexPath animated:YES];
                     _userDefaults = [NSUserDefaults standardUserDefaults];
                     [WBAPIClient deleteAccauntWithComplete:^(id responseObject, NSError *error) {
                         if (error)
                         {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                             });
                             NSLog(@"%@", [error.userInfo objectForKey:@"message"]);
                             [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error", nil) message:DPLocalizedString(@"repeatOperationLater", nil)
                                               cancelButtonTitle:DPLocalizedString(@"close", nil)]
                              showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 1)
                                  {
                                      // delete
                                  }
                              }];
                         }
                         else
                         {
                             User* localUser = [WBParser getUser:responseObject andEmail:self.textFieldEmail.text];
                             NSLog(@"%@", localUser.userSID);
                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                             [userDefaults setObject:[responseObject objectForKey:@"SID"] forKey:@"SID"];
                             [userDefaults synchronize];
                             [self.navigationController popViewControllerAnimated:NO];
                             AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                             
                             [UIView transitionWithView:appDelegateTemp.window duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                                 appDelegateTemp.window.rootViewController  = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
                             } completion:nil];
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                             });
                             
                         }
  
                     }];
                 }
             }];
        }
            break;
    }
}

-(void)hideChangePasswordView
{
    [_popupChangePassword dismiss:YES];
}

@end
