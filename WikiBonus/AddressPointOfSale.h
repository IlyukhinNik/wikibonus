//
//  AdressPointOfSale.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/4/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressPointOfSale : NSObject

@property(nonatomic) NSInteger countryID;
@property(nonatomic) NSInteger cityID;
@property(nonatomic, strong) NSString *cityName;
@property(nonatomic, strong) NSString *address;

-(NSString*)addressToString;
-(void)updateAddressPointOfSaleWhithCountryID:(NSInteger)countryID cityID:(NSInteger)cityID cityName:(NSString*)cityName address:(NSString*)address;
@end
