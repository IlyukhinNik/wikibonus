//
//  HallOfFameViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HallOfFameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewHallOfFame;

@end
