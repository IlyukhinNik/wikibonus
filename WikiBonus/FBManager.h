//
//  FBManager.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 3/3/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBManager : NSObject

-(BOOL)logIn;
-(void)logOut;

-(NSDictionary*)getUserData;
-(void)shareFBText:(NSString*)text andUrl:(NSURL*)url andVC:(UIViewController*)vc;

@end
