//
//  TakeGiftSplashViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 1/28/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Gift.h"
#import "Fragmen.h"

@interface TakeGiftSplashViewController : UIViewController

@property(nonatomic) Gift* gift;
@property(nonatomic) Fragmen* fragment;

@end
