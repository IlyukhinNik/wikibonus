//
//  Partner.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/17/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryPointOfSale.h"
#import "AddressPointOfSale.h"


@interface PointOfSale : NSObject

@property (nonatomic) NSInteger idPointOfSale;
@property (nonatomic) NSInteger idCompany;
@property (nonatomic, strong) NSString *namePointOfSale;
@property (nonatomic, strong) AddressPointOfSale *addressPointOfSale;
@property (nonatomic) CLLocationCoordinate2D coordinatesPointOfSale;
@property (nonatomic) float starsCount;
@property (nonatomic, strong) CategoryPointOfSale *categotyPointOfSale;
@property (nonatomic, strong) UIImage *imageLogoPointOfSale;
@property (nonatomic, strong) NSString *aboutCompany;
@property (nonatomic, strong) NSString *descriptionCompany;
@property (nonatomic, strong) NSDictionary *contactsDictionary;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic) NSArray *socialArray;
@property (nonatomic) BOOL pointLock;


- (void)updatePointOfSaleForMapViewWithID:(NSInteger)idPointOfSale idCompany:(NSInteger)idCompany namePointOfSale:(NSString*)pointOfSaleName pointOfSaleAddress:(AddressPointOfSale*)pointOfSaleAddress coordinatePointOfSale:(CLLocationCoordinate2D) coordinatePointOfSale category:(CategoryPointOfSale*)categoryPointOfSale starsRate:(float)starsRate aboutCompanyString:(NSString*)aboutCompany;

-(void)updatePointWithMissingInfoLogoImage:(UIImage*)imageLogo description:(NSString*)description contacts:(NSDictionary*)contactsDictionary socialURLArray:(NSMutableArray*)socialURLArray;

-(void)updatePointWithCompanyInfoDictionary:(NSDictionary*)companyInfo;
@end
