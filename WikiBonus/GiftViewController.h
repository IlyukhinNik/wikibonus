//
//  GiftViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/15/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Gift.h"

@interface GiftViewController : UIViewController

@property(strong, nonatomic) Gift *gift;

@end
