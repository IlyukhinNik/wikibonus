//
//  QRScannerViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 2/8/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "QRScannerViewController.h"
#import "RSCodeView.h"

@interface QRScannerViewController ()

@end

@implementation QRScannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setStopOnFirst:YES];
        [self setIsBorderRectsVisible:YES];
        self.barcodesHandler = ^(NSArray *barcodeObjects) {
            NSLog(@"%lu", (unsigned long)barcodeObjects.count);
            if (barcodeObjects.count > 0) {
                NSMutableString *text = [[NSMutableString alloc] init];
                [barcodeObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    [text appendString:[NSString stringWithFormat:@"%@: %@", [(AVMetadataObject *)obj type], [obj stringValue]]];
                    if (idx != (barcodeObjects.count - 1)) {
                        [text appendString:@"\n"];
                    }
                }];
                dispatch_async(dispatch_get_main_queue(), ^{

                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Barcode found"
                                                                    message:text
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    [self stopRunning];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    weakSelf.codeLabel.text = @"";
                    
                });
            }
        };
        
        self.tapGestureHandler = ^(CGPoint tapPoint) {
        };
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
