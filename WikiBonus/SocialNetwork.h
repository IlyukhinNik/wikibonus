//
//  SocialNetwork.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 2/26/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VKManager.h"
#import "FBManager.h"
#import "GPManager.h"

@interface SocialNetwork : NSObject

@property (nonatomic) VKManager* vkSocManager;
@property (nonatomic) FBManager* fbSocManager;
@property (nonatomic) GPManager* gpSocManager;


-(void)logOut;
-(BOOL)logInWithSocNet:(NSString*)socNet;

@end
