//
//  User.h
//  
//
//  Created by Nikita Ilyukhin on 10/29/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSObject

@property (nonatomic, strong) NSString *firstNameUser;
@property (nonatomic, strong) NSString *lastNameUser;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *emailUser;
@property (nonatomic, strong) NSString *socialNetUser;
@property (nonatomic, strong) NSString *cityUser;
@property (nonatomic, strong) NSString *userLock;
@property (nonatomic, strong) NSString *phoneUser;
@property (nonatomic, strong) NSDate *bdayUser;
@property (nonatomic, strong) NSString *photoURL;
@property (nonatomic, strong) NSString *photoLocalURL;
@property (nonatomic, strong) NSString *userSocURL;
@property (nonatomic, strong) NSString *userSID;

@end

