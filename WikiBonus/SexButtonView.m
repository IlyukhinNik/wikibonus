//
//  SexButtonView.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/30/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "SexButtonView.h"

@implementation SexButtonView



- (void)drawRect:(CGRect)rect{
    [WikiBonusStyleKit drawSexButtonWithFrame:rect pressed:self.isPressed sex:self.sexString];
}
-(void)setButtonTitleWithString:(NSString*)buttonTitle
{
    self.sexString = buttonTitle;
}
-(void)enable
{
    self.isPressed = YES;
}
-(void)isActive
{
    self.isPressed = YES;
    [self setNeedsDisplay];
}

-(void)isNoActive
{
    self.isPressed = NO;
    [self setNeedsDisplay];
}

@end
