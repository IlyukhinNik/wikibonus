//
//  FBManager.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 3/1/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "FBManager.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@implementation FBManager

-(BOOL)logIn
{
    __block BOOL _isLogIn;
    if ([FBSDKAccessToken currentAccessToken])
    {
        _isLogIn = YES;
    }
    else
    {
        [[FBSDKLoginManager new] logInWithReadPermissions: @[@"public_profile", @"email", @"user_about_me", @"user_birthday", @"user_location"] fromViewController:nil
                                                  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                                      if (error)
                                                      {
                                                          _isLogIn = NO;
                                                          NSLog(@"Process error");
                                                          [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации Facebook" message:@"Повторите действие позже."
                                                                            cancelButtonTitle:@"Закрыть"]
                                                           showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                           }];
                                                      }
                                                      else if (result.isCancelled)
                                                      {
                                                          _isLogIn = NO;
                                                          NSLog(@"Cancelled");
                                                          [[[UIAlertView alloc] initWithTitle:@"Отмена авторизации Facebook" message:@"Повторите действие позже."
                                                                            cancelButtonTitle:@"Закрыть"]
                                                           showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                           }];
                                                      }
                                                      else
                                                      {
                                                          _isLogIn = YES;
                                                          NSLog(@"Logged in");

                                                      }
                                                  }];
    }
    
    return _isLogIn;
}


-(void)shareFBText:(NSString*)text andUrl:(NSURL*)url andVC:(UIViewController*)vc
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentDescription = text;
    content.contentURL = [NSURL
                          URLWithString:@"https://www.facebook.com/FacebookDevelopers"];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = vc;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeShareSheet;
    [dialog show];

}

-(NSDictionary*)getUserData
{
    __block NSDictionary* _userData = [NSDictionary new];
    if(([FBSDKAccessToken currentAccessToken]) != nil)
    {
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, first_name, last_name, email, gender, birthday, location, link"}];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 _userData = (NSDictionary *)result;
             }
         }];
    }
    
    return _userData;
}

-(void)logOut
{
    [[FBSDKLoginManager new] logOut];
}

@end
