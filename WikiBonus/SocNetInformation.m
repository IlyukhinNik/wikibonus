//
//  socNetInformation.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/11/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "SocNetInformation.h"

@implementation SocNetInformation


-(void)updateSocNetInformationWithName:(NSString*)nameSocNet linkSocNet:(NSString*)linkSocNet imageSocNet:(UIImage*)imageSocNet
{
    self.nameSocNet = nameSocNet;
    self.linkSocNet = linkSocNet;
    self.imageSocNet = imageSocNet;
}


@end
