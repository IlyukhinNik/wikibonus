//
//  MyDesiresViewCOntroller.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "MyDesiresViewCOntroller.h"
#import "MyDesiresTableViewCell.h"

@interface MyDesiresViewCOntroller ()
{
    NSArray *_desiresArray;
}

@end

@implementation MyDesiresViewCOntroller

- (void)viewDidLoad
{
    [super viewDidLoad];
    _desiresArray = @[@"Тортик в подарок", @"Пироженко в подарок"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _desiresArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 118;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyDesiresTableViewCell* cell = [self.tableViewMyDesires dequeueReusableCellWithIdentifier:@"cellDesire"];
    if (cell == nil)
    {
        cell = [[MyDesiresTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellDesire"];
    }
    cell.labelNameDesire.text = [_desiresArray objectAtIndex:indexPath.row];
    UIImage *image = [UIImage imageNamed:@"gift.png"];
    cell.imageViewPhotoDesire.image = image;
    return cell;
}
@end
