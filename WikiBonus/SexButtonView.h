//
//  SexButtonView.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/30/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SexButtonView : UIView

@property BOOL isPressed;
@property (copy) NSString* sexString;

-(void)isActive;
-(void)isNoActive;
-(void)enable;
-(void)setButtonTitleWithString:(NSString*)buttonTitle;

@end
