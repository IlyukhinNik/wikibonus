//
//  Gift.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/11/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "Gift.h"

@implementation Gift

-(void)updateGiftWithIDGift:(NSInteger)idGift nameGift:(NSString*)nameGift description:(NSString*)description image:(UIImage*)image likesCount:(NSInteger)likesCount reviewsCount:(NSInteger)reviewsCount idPointOfSale:(NSInteger)idPointOfSale dateDead:(NSDate*)dateDead arrayFragments:(NSArray*)arrayFragments inWishes:(BOOL)inWishes isRecieved:(BOOL)isRecieved procentFull:(float)procentFull
{
    self.idGift = idGift;
    self.nameGift = nameGift;
    self.descriptionGift = description;
    self.imageGift = image;
    self.likesCount = likesCount;
    self.reviewsCount = reviewsCount;
    self.idPointOfSale = idPointOfSale;
    self.dateDead =dateDead;
    self.arrayFragments = arrayFragments;
    self.inWishes = inWishes;
    self.isReceived = isRecieved;
    self.procentFull = procentFull;
}
@end
