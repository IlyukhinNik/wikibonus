//
//  AutorizationViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "AutorizationViewController.h"
#import "RegistrationViewController.h"
#import "User.h"
#import "WBAPIClient.h"
#import "WBParser.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

//static NSString *const TOKEN_KEY = @"dhna2crr0PbTME84T7Jh";
static NSString *const NEXT_CONTROLLER_SEGUE_ID = @"startWork";
static NSArray  * SCOPE = nil;

@interface AutorizationViewController () <VKSdkDelegate, VKSdkUIDelegate>
{
    VKAccessToken* _vkAccessToken;
    User* _localUser;
    NSDictionary* _dictionaryLocalUserInfo;
}

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;

@end

@implementation AutorizationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.textFieldEmail setPlaceholder:DPLocalizedString(@"email", nil)];
    [self.textFieldPassword setPlaceholder:DPLocalizedString(@"password", nil)];
    SCOPE = @[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_EMAIL];
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:VK_APP_KEY];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - working with PList methods

- (void)loadUserDataFromPList
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths firstObject];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path])
    {
        NSLog(@"Path doesn't exist. plist file will be copied to the path.");
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"UserInfo" ofType:@"plist"];
        if (bundlePath)
        {
            NSLog(@"File exists in the main bundle.");
            NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:bundlePath];
            NSLog(@"Bundle UserInfo.plist, %@", [resultDictionary description]);
            [fileManager copyItemAtPath:bundlePath toPath:path error:nil];
            NSLog(@"Plist file is copied from main bundle to document directory");
        }
        else
        {
            NSLog(@"UserInfo.plist not found in main bundle.");
        }
    }
    NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    NSLog(@"Loaded UserInfo.plist, %@", [resultDictionary description]);
    if (resultDictionary)
    {
        _localUser.firstNameUser = [resultDictionary objectForKey:@"firstNameUser"];
    }
    else
    {
        NSLog(@"WARNING: Couldn't create dictionary from UserInfo.plist at Documents Dicretory");
    }
}

- (void)saveUserDataToPListWithDictionary:(NSDictionary*)dictionaryResponse
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
    [dictionaryResponse writeToFile:path atomically:YES];
    NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    NSLog(@"Saved UserInfo.plist, %@", [resultDictionary description]);
}

#pragma mark - working with VK methods
-(void) getVKUserData
{
    [self.delegate showProgressHUD];
        VKRequest* dataRequest = [[VKApi users] get:@{@"fields": @"id, first_name, last_name, sex, photo_max_orig, email, city, bdate"}];
        [dataRequest executeWithResultBlock:^(VKResponse *response)
        {
            User *localUser = [User new];
            NSMutableDictionary *dict = [response.json objectAtIndex:0];
            localUser = [WBParser getUserFromDictionaryVK:dict];
            localUser.emailUser = _vkAccessToken.email;
            NSLog(@"%@", localUser.photoURL);
            if (localUser.emailUser)
            {
                NSDateFormatter *dateFormatter = [NSDateFormatter new];
                [dateFormatter setDateFormat:@"YYYY-MM-dd"];
                
                [WBAPIClient socNetRegistrationWithUser:localUser complete:^(id responseObject, NSError *error){
                    if (error)
                    {
                        NSLog(@"%@", error.localizedDescription);
                        [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации" message:@"Повторите действие позже."
                                          cancelButtonTitle:@"Закрыть"]
                         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                             if (buttonIndex == 1)
                             {
                                 // delete
                             }
                         }];
                    }
                    else
                    {
                        User* locUser = [WBParser getUser:responseObject andEmail:localUser.emailUser];
                        NSLog(@"%@", locUser.userSID);
                        [self saveUserDataToPListWithDictionary:[WBParser makeDictionaryForPList:responseObject andEmail:localUser.emailUser]];
                        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                        [userDefaults setObject:[responseObject objectForKey:@"SID"] forKey:@"SID"];
                        [userDefaults synchronize];
                        [self.navigationController popViewControllerAnimated:NO];
                        AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                        
                        [UIView transitionWithView:appDelegateTemp.window duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                            appDelegateTemp.window.rootViewController  = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
                        } completion:nil];
                    }
                }
                 ];
                
            }
            else
            {
                [self.delegate hideProgressHUD];
                [self.delegate showRegViewControllerWithParamName:localUser.firstNameUser gender:localUser.gender];
                [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации" message:@"В вашем профиле не указан e-mail, пожалуйста, пройдите регистрацию"
                                  cancelButtonTitle:@"Закрыть"]
                 showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                     if (buttonIndex == 1)
                     {
                         // delete
                     }
                 }];
            }
        } errorBlock:^(NSError *error) {
            [self.delegate hideProgressHUD];
            [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации VK" message:@"Повторите действие позже."
                              cancelButtonTitle:@"Закрыть"]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 1)
                 {
                     // delete
                 }
             }];
        }];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    if (!result.error)
    {
        _vkAccessToken = result.token;
        [self getVKUserData];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации VK" message:@"Повторите действие позже."
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         }];
    }
    
}

- (void)vkSdkUserAuthorizationFailed
{
    [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации VK" message:@"Повторите действие позже."
                      cancelButtonTitle:@"Закрыть"]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
     }];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.navigationController.topViewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    [self pressedButtonVKRegistration:nil];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    _vkAccessToken = newToken;
    NSLog(@"%@", _vkAccessToken.userId);
    [self getVKUserData];
}
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    [[[UIAlertView alloc] initWithTitle:nil message:@"Доступ запрещен" delegate:self cancelButtonTitle:@"Закрыть" otherButtonTitles:nil] show];
}

#pragma mark - working with FB methods

-(void) getFBUserData
{
    if(([FBSDKAccessToken currentAccessToken]) != nil)
    {
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, first_name, last_name, email, gender, birthday, location, link"}];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 NSDictionary *userData = (NSDictionary *)result;
                 
                 
                 User *localUser = [User new];
                 localUser = [WBParser getUserFromDictionaryFB:userData];
            
                 if(localUser.emailUser)
                 {
                     [self.delegate showProgressHUD];
                     NSDateFormatter *dateFormatter = [NSDateFormatter new];
                     [dateFormatter setDateFormat:@"YYYY-MM-dd"];
                     [WBAPIClient socNetRegistrationWithUser:localUser complete:^(id responseObject, NSError *error){
                          if (error)
                          {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                              });
                              
                              NSLog(@"%@", [error.userInfo objectForKey:@"message"]);
                              [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации" message:@"Повторите действие позже."
                                                cancelButtonTitle:@"Закрыть"]
                               showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                   if (buttonIndex == 1)
                                   {
                                       // delete
                                   }
                               }];
                          }
                          else
                          {
                              User* locUser = [WBParser getUser:responseObject andEmail:localUser.emailUser];
                              NSLog(@"%@", locUser.userSID);
                              [self saveUserDataToPListWithDictionary:[WBParser makeDictionaryForPList:responseObject andEmail:localUser.emailUser ]];
                              NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                              [userDefaults setObject:[responseObject objectForKey:@"SID"] forKey:@"SID"];
                              [userDefaults synchronize];
                              [self.navigationController popViewControllerAnimated:NO];
                              AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                              [self.delegate hideProgressHUD];
                              [UIView transitionWithView:appDelegateTemp.window duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                                  appDelegateTemp.window.rootViewController  = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
                              } completion:nil];
                          }
                      }
                      ];
                 }
                 else
                 {
                     [self.delegate hideProgressHUD];
                     
                     [self.delegate showRegViewControllerWithParamName:localUser.firstNameUser gender:localUser.gender];
                     [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации" message:@"В вашем профиле не указан e-mail, пожалуйста, пройдите регистрацию"
                                       cancelButtonTitle:@"Закрыть"]
                      showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == 1)
                          {
                              // delete
                          }
                      }];
                     
                 }
             }
         }];
    }
}

#pragma mark - verification TextField methods

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)validateTextFieldLegthEmail:(NSString*)email andPassword:(NSString*)password
{
    BOOL _isValidate;
    if (email.length < 50 && password.length < 50)
    {
        _isValidate = YES;
    }
    else
    {
        _isValidate = NO;
    }
    return _isValidate;
}

- (IBAction)pressedButtonFBRegistration:(id)sender
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [self getFBUserData];
    }
    else
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions: @[@"public_profile", @"email", @"user_about_me", @"user_birthday", @"user_location"] fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error)
             {
                 NSLog(@"Process error");
                 [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации Facebook" message:@"Повторите действие позже."
                                   cancelButtonTitle:@"Закрыть"]
                  showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                  }];
             }
             else if (result.isCancelled)
             {
                 NSLog(@"Cancelled");
                 [[[UIAlertView alloc] initWithTitle:@"Отмена авторизации Facebook" message:@"Повторите действие позже."
                                   cancelButtonTitle:@"Закрыть"]
                  showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                  }];
             }
             else
             {
                 NSLog(@"Logged in");
                 [self getFBUserData];
                 
             }
         }];
    }
}

-(BOOL)verificationOfFields
{
    if (![self validateEmailWithString:self.textFieldEmail.text])
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Введите корректный e-mail адрес"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
        {
        }];
        return NO;
    }
    if (self.textFieldPassword.text.length<6)
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Пароль должен содержать не менее 6 символов!"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
        {
        }];
        return NO;
    }
    return YES;
}

#pragma mark - buttons pressed methods

- (IBAction)pressedButtonVKRegistration:(id)sender
{
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized)
        {
            _vkAccessToken = [VKSdk accessToken];
            [self getVKUserData];
        }
        else if (state == VKAuthorizationInitialized)
        {
            [VKSdk authorize:SCOPE];
        }
    }];
}

- (IBAction)pressedButtonLogin:(id)sender
{
    if ([self verificationOfFields])
    {
        [self.delegate showProgressHUD];
        [WBAPIClient authLoginWithEmail:self.textFieldEmail.text andPassword:self.textFieldPassword.text complete:^(id responseObject, NSError *error) {
            if (error)
            {
                [self.delegate hideProgressHUD];
                NSLog(@"%@", [error.userInfo objectForKey:@"message"]);
                [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации" message:@"Повторите действие позже."
                                  cancelButtonTitle:@"Закрыть"]
                 showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                     if (buttonIndex == 1)
                     {
                         // delete
                     }
                 }];
            }
            else
            {
                User* localUser = [WBParser getUser:responseObject andEmail:self.textFieldEmail.text];
                NSLog(@"%@", localUser.userSID);
                [self saveUserDataToPListWithDictionary:[WBParser makeDictionaryForPList:responseObject andEmail:self.textFieldEmail.text]];
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:[responseObject objectForKey:@"SID"] forKey:@"SID"];
                [userDefaults synchronize];
                [self.navigationController popViewControllerAnimated:NO];
                AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                
                [UIView transitionWithView:appDelegateTemp.window duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                    appDelegateTemp.window.rootViewController  = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
                } completion:nil];
                [self.delegate hideProgressHUD];
            }
        }];
    }
}

@end
