//
//  popUpView.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/19/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "PopUpView.h"
#import "RestaurantViewController.h"

@implementation PopUpView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [WikiBonusStyleKit drawPopupMapWithFrame:self.bounds];
    UIGraphicsBeginImageContextWithOptions(self.imageViewRate.bounds.size, NO, scaleScreen);
    [WikiBonusStyleKit drawPointOfSaleStarRateWithFrame:self.imageViewRate.bounds starsCount:self.starRate];
    [self.imageViewRate setImage: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:singleFingerTap];
}

-(void)updatePopUpViewDataWithPinAnnotation:(MKAnnotationCustom*)annotationCustom
{
    self.pointOfSale = annotationCustom.pointOfSale;
    self.labelTitle.text = annotationCustom.pointOfSale.namePointOfSale;
    self.labelAddress.text = [annotationCustom.pointOfSale.addressPointOfSale addressToString];
    self.starRate = annotationCustom.pointOfSale.starsCount;
}

- (IBAction)buttonDetailInfoPressed:(id)sender
{
    [self.delegate showPointOfSaleViewControllerWithPointOfSale:self.pointOfSale];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self.delegate showPointOfSaleViewControllerWithPointOfSale:self.pointOfSale];
}


@end
 