//
//  Defines.h
//  WikiBonus
//
//  Created by Dima on 27.10.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

#define WB_PROPERTY_CELL @"propertyCell"
#define WB_CONTACT_CELL @"contactCell"

#define WB_BASE_URL @"https://api.wikibonus.ru/ru/"
#define WB_AUTH_URL @"auth.json/"
#define WB_BUSINESS_CAT @"lists.json/business_category/"
#define WB_GEO_SEARCH @"point.json/geo/"
#define WB_POINTS_LIST @"point.json/list/"
#define WB_COMPANY @"company.json/id/"
#define WB_REG_URL @"register.json/client/"
#define WB_REG_URL_SOC_NET @"register.json/soc/"
#define WB_CLIENT @"client.json/"
#define WB_AUTH_FIRST_NAME @"first_name"
#define WB_SID @"SID"
#define WB_AUTH_LAST_NAME @"last_name"
#define WB_AUTH_GENDER @"sex"
#define WB_SOC_NET_URL @"soc_url"
#define WB_SOC_NET @"soc_net"
#define WB_AUTH_CITY @"city_name"
#define WB_AUTH_BDATE @"birthday"
#define WB_AUTH_PHOTO_URL @"avatar"
#define REUSABLE_ANNOTATION_VIEW_IDENTIFIER @"CustomAnnotationViewIdentifier"
#define WB_AUTH_EMAIL @"email"
#define WB_AUTH_PASSWORD @"psw"
#define WB_AUTH_LOGIN @"login"

#define WB_AUTH_KEY @"kjaWGLljaskhLl6SLHJh89hy8HioHuIOHLIoyJBuyRgdhjasKG"
#define VK_APP_KEY @"5124735"
#define scaleScreen [UIScreen mainScreen].scale
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 alpha:1.0]

#define APP_NAME @"Map Example"
#define  NO_NAME_TEXT @"No Name"
#define  PIN_GRAPHIC_FILENAME @"pin.png"

#endif /* Defines_h */
