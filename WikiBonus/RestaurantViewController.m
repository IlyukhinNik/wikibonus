//
//  RestaurantViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/19/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "MKAnnotationCustom.h"
#import "RestaurantViewController.h"
#import "PartnersPinAnnotationSettings.h"
#import "PartnersPinAnnotationView.h"
#import "SocNetInformation.h"
#import "CollectionViewCellGift.h"
#import "Gift.h"
#import "Fragmen.h"
#import "GiftViewController.h"
#import "UIButton+VerticalLayout.h"
#import "ImageViewBackgroundBorder.h"

@interface RestaurantViewController () <MKMapViewDelegate,CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
{
    NSUInteger _countOfRows;
    NSMutableArray* _socialNetwoksArray;
    NSMutableArray* _giftsArray;
    NSMutableArray* _testFragmentArray;
    Gift *_currentGift;
    IBOutlet ImageViewBackgroundBorder *imageViewBackgroundNameDescription;
    IBOutlet ImageViewBackgroundBorder *imageViewBackgroundBorder;
}
@property (weak, nonatomic) IBOutlet MKMapView *restaurantMapView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRestaurantLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRate;
@property (weak, nonatomic) IBOutlet UIButton *buttonDetailInformation;
@property (weak, nonatomic) IBOutlet UIButton *buttonMakeRoad;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewGifts;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPlaces;
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;

@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (nonatomic) NSArray *pointsOfSaleAdresses;
@property (nonatomic) NSMutableArray *restaurantContactInfo;

@property (weak, nonatomic) IBOutlet UILabel *labelNamePointOfSale;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTableViewHeight;

@end

@implementation RestaurantViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTestFragments];
    [self setTestGifts];
    self.tableViewPlaces.layer.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    self.tableViewPlaces.layer.borderWidth = 0.5f;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    // prompt for location allowing
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    else
    {
    }
    [self.locationManager startUpdatingLocation];

    self.restaurantMapView.delegate = self;
    self.restaurantMapView.mapType = MKMapTypeStandard;
    self.restaurantMapView.showsUserLocation = YES;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = self.pointsOfSale.coordinatesPointOfSale.latitude;
    location.longitude = self.pointsOfSale.coordinatesPointOfSale.longitude;
    region.span = span;
    region.center = location;
    [self.restaurantMapView setRegion:region animated:YES];
    
    MKAnnotationCustom *annotationPartner = [[MKAnnotationCustom alloc] initWithPointOfSale:self.pointsOfSale];
    [self.restaurantMapView addAnnotation:annotationPartner];
    [self mapView:self.restaurantMapView viewForAnnotation:annotationPartner];
    
    // set up test data
    
    self.labelNamePointOfSale.text = self.pointsOfSale.namePointOfSale;
    self.navigationController.topViewController.title = self.pointsOfSale.namePointOfSale;
    
    self.pointsOfSaleAdresses = @[@"г.Днепропетровск, ул.Сладкоежкина, 23", @"г.Днепропетровск, пр.Конфетно-кексовый, 2/1", @"г.Днепропетровск,Новомосковск, пер.Кисельный, 23б", @"г.Днепропетровск, г.Днепропетровск, пр.Правды, 24"];
    
    _socialNetwoksArray = [NSMutableArray new];
    [self testSocNetInformationToArray:_socialNetwoksArray];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(25, 25), NO, scaleScreen);
    [WikiBonusStyleKit drawInfoProductPageIconWithFrame:CGRectMake(0, 0, 25, 25)];
     [self.buttonDetailInformation setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
    [self.buttonDetailInformation centerVertically];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(25, 25), NO, scaleScreen);
    [WikiBonusStyleKit drawLocationIconWithFrame:CGRectMake(0, 0, 25, 25)];
    [self.buttonMakeRoad setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
    [self.buttonMakeRoad centerVertically];
    
    UIGraphicsBeginImageContextWithOptions(self.imageViewRate.bounds.size, NO, scaleScreen);
    [WikiBonusStyleKit drawPointOfSaleStarRateWithFrame:self.imageViewRate.bounds starsCount:self.pointsOfSale.starsCount];
    [self.imageViewRate setImage: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    

    NSDictionary *dictionaryContactsInformation = @{@"address" :[self.pointsOfSale.addressPointOfSale addressToString],
                                                    @"phone" : @"+79097777777",
                                                    @"email": @"mail@mail.com",
                                                    @"mphone": @"+79097777777"};
    
    [self.pointsOfSale updatePointWithMissingInfoLogoImage:nil description:@"Подробное описание iugvlr.njvenoecubenvlkewrgjemniouegbywgoirhgekrjvbevearbvri iut ut34ptuh34fk i34ut 234upit45ghuirfh 5ith34iufgrlaiughq3lrughl3;iu3rfghqr3;guhqr39;p" contacts:dictionaryContactsInformation socialURLArray:_socialNetwoksArray];

    NSMutableAttributedString *descriptionAttributedString = [[NSMutableAttributedString alloc] initWithString:self.pointsOfSale.descriptionCompany];
    NSMutableAttributedString *addressAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\nтел.:%@\nмоб.:%@\nemail:%@",
                                                                                                     [dictionaryContactsInformation objectForKey:@"address"],
                                                                                                     [dictionaryContactsInformation objectForKey:@"phone"],
                                                                                                     [dictionaryContactsInformation objectForKey:@"mphone"] ,
                                                                                                     [dictionaryContactsInformation objectForKey:@"email"]]];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;
    [addressAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, addressAttributedString.length)];
    [descriptionAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, addressAttributedString.length)];
    
    self.labelDescription.attributedText = descriptionAttributedString;
    self.labelAddress.attributedText = addressAttributedString;
    
    _countOfRows = self.pointsOfSaleAdresses.count + self.pointsOfSale.socialArray.count;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - set test information

-(void)setTestGifts
{
    _giftsArray = [NSMutableArray new];
    Gift *testGift1= [Gift new];
    [testGift1 updateGiftWithIDGift:1 nameGift:@"Тортик в подарок" description:@"Соберите 3 фрагмента и получите в подарок бесплатный тортик от кондитерской Карамель" image:[UIImage imageNamed:@"gift.png"] likesCount:12 reviewsCount:3 idPointOfSale:1 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:NO procentFull:0.75];
    [_giftsArray addObject:testGift1];
    Gift *testGift2= [Gift new];
    [testGift2 updateGiftWithIDGift:2 nameGift:@"Пироженко в подарок" description:@"Соберите все фрагменты и получите пироженко в подарок" image:[UIImage imageNamed:@"gift.png"] likesCount:4 reviewsCount:1 idPointOfSale:3 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:YES procentFull: 1.0];
    [_giftsArray addObject:testGift2];
    Gift *testGift3= [Gift new];
    [testGift3 updateGiftWithIDGift:3 nameGift:@"Кофе в подарок" description:@"Соберите все фрагменты и получите кофе в подарок" image:[UIImage imageNamed:@"gift.png"] likesCount:4 reviewsCount:1 idPointOfSale:3 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:NO procentFull: 0.0];
    [_giftsArray addObject:testGift3];
}

-(void)setTestFragments
{
    _testFragmentArray = [NSMutableArray new];
    Fragmen* testFragment1 = [Fragmen new];
    [testFragment1 updateFragmentWithID:1 nameFragment:@"First" imageFragment:[UIImage imageNamed:@"fragment1.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment1];
    Fragmen* testFragment2 = [Fragmen new];
    [testFragment2 updateFragmentWithID:1 nameFragment:@"Second" imageFragment:[UIImage imageNamed:@"fragment2.png"] dateDead:[NSDate date] isReceived:NO];
    [_testFragmentArray addObject:testFragment2];
    Fragmen* testFragment3 = [Fragmen new];
    [testFragment3 updateFragmentWithID:1 nameFragment:@"Third" imageFragment:[UIImage imageNamed:@"fragment3.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment3];
    Fragmen* testFragment4 = [Fragmen new];
    [testFragment4 updateFragmentWithID:1 nameFragment:@"Fourth" imageFragment:[UIImage imageNamed:@"fragment4.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment4];
}



-(void)testSocNetInformationToArray:(NSMutableArray*)testInfoArray
{
    NSArray *tmpSocialNameArray = @[@"www.parner.ua",@"partner@partner.ua",@"Facebook",@"Vkontakte",@"Twitter", @"Instagram"];
    NSArray* tmpSocialLinkArray = @[@"www.cuborubo.com",@"",@"www.facebook.com",@"www.vk.com",@"www.twitter.com", @"www.instagram.com"];
    NSMutableArray *tmpImageArray = [NSMutableArray new];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawWebIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [tmpImageArray addObject: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawEmailContactWithFrame:CGRectMake(0, 0, 30, 30)];
    [tmpImageArray addObject: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawFbIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [tmpImageArray addObject: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawVkIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [tmpImageArray addObject: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawTwitterIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [tmpImageArray addObject: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawInstagramIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [tmpImageArray addObject: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    for (NSUInteger i = 0; i < tmpImageArray.count; i++)
    {
        SocNetInformation* tmpSocNetInfo = [SocNetInformation new];
        [tmpSocNetInfo updateSocNetInformationWithName:[tmpSocialNameArray objectAtIndex:i] linkSocNet:[tmpSocialLinkArray objectAtIndex:i] imageSocNet:[tmpImageArray objectAtIndex:i]];
        [testInfoArray addObject:tmpSocNetInfo];
    }
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return [self.pointsOfSaleAdresses count];

    else
        return [self.pointsOfSale.socialArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [self.tableViewPlaces dequeueReusableCellWithIdentifier:@"cellRestaurantInfo"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellRestaurantInfo"];
    }
    if (indexPath.section == 0)
    {
       cell.textLabel.text = [self.pointsOfSaleAdresses objectAtIndex:indexPath.row];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
        [WikiBonusStyleKit drawPinProductIconWithFrame:CGRectMake(0, 0, 30, 30)];
        cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    if (indexPath.section == 1)
    {
        SocNetInformation * tmpSocNet = [self.pointsOfSale.socialArray objectAtIndex:indexPath.row];
        cell.textLabel.text = tmpSocNet.nameSocNet;
        cell.imageView.image = tmpSocNet.imageSocNet;
    }
    
    cell.textLabel.font=[UIFont systemFontOfSize:10.0];
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @" ";
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *v = (UITableViewHeaderFooterView *)view;
    v.backgroundView.backgroundColor = [UIColor whiteColor];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"inDevelopment", nil) message:nil
                      cancelButtonTitle:DPLocalizedString(@"close", nil)]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if (buttonIndex == 1)
         {
             // delete
         }
     }];
}

#pragma mark - MapView methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isKindOfClass:[MKAnnotationCustom class]])
    {
        UIImageView *pinView = nil;
        PartnersPinAnnotationView *annotationView = (PartnersPinAnnotationView *)[self.restaurantMapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([PartnersPinAnnotationView class])];
        MKAnnotationCustom *pinAnnotation = (MKAnnotationCustom*) annotation;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(40, 40), NO, scaleScreen);
        [WikiBonusStyleKit drawPinDisableWithFrame:CGRectMake(0, 0, 40, 40) pressed:NO procentFull:pinAnnotation.procent];
        UIImage *pinImage = UIGraphicsGetImageFromCurrentImageContext();
        annotationView.image = pinImage;
        UIGraphicsEndImageContext();
        pinView = [[UIImageView alloc] initWithImage:pinImage];
        annotationView = [[PartnersPinAnnotationView alloc] initWithAnnotation:annotation
                                                               reuseIdentifier:NSStringFromClass([PartnersPinAnnotationView class])
                                                                       pinView:pinView
                                                                   calloutView:nil
                                                                      settings:[PartnersPinAnnotationSettings defaultSettings]];

        return annotationView;
    }
    return nil;
}

#pragma mark - CollectionView methods

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return _giftsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCellGift *myCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"giftCell"
                                    forIndexPath:indexPath];
    Gift *tmpGift = [Gift new];
    tmpGift = [_giftsArray objectAtIndex:indexPath.row];
    myCell.imageViewGift.image = tmpGift.imageGift;
    myCell.labelNameGift.text = tmpGift.nameGift;
    
    if(tmpGift.procentFull > 0.0)
    {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(76, 76), NO, scaleScreen);
        [WikiBonusStyleKit drawProcentGiftWithProcentFull:tmpGift.procentFull dem: 74  strokeWidth:2.0];
        myCell.imageViewProcentGift.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        myCell.labelProcentGift.text = [NSString stringWithFormat:@"%d%%", (int)ceilf(tmpGift.procentFull*100)];
    }
    return myCell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"showGift"])
    {
        NSIndexPath *path = [self.collectionViewGifts indexPathForCell:(UICollectionViewCell*)sender];
        _currentGift = [_giftsArray objectAtIndex:path.row];
        GiftViewController *giftVC = [segue destinationViewController];
        giftVC.gift = _currentGift;
        
    }
}


//Open apple maps with params to navigate
- (IBAction)makeRoadButtonPressed:(id)sender
{
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.pointsOfSale.coordinatesPointOfSale.latitude, self.pointsOfSale.coordinatesPointOfSale.longitude) addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:self.pointsOfSale.namePointOfSale];
    [mapItem openInMapsWithLaunchOptions:nil];
}

- (IBAction)detailInformationButtonPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"inDevelopment", nil) message:nil
                      cancelButtonTitle:DPLocalizedString(@"close", nil)]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if(buttonIndex == 1)
         {
             // delete
         }
     }];
}

- (IBAction)shareButtonPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"inDevelopment", nil) message:nil
                      cancelButtonTitle:DPLocalizedString(@"close", nil)]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if (buttonIndex == 1)
         {
             // delete
         }
     }];
}
@end
