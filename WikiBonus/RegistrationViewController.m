//
//  RegistrationViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "RegistrationViewController.h"
#import "SexButton.h"
#import "WBAPIClient.h"
#import "MBProgressHUD.h"

static NSString *const NEXT_CONTROLLER_SEGUE_ID = @"regAndStartWorking";

@interface RegistrationViewController () <UIGestureRecognizerDelegate>

@end

@implementation RegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.textFieldName setPlaceholder:DPLocalizedString(@"name", nil)];
    [self.textFieldEmail setPlaceholder:DPLocalizedString(@"email", nil)];
    [self.textFieldPassword setPlaceholder:DPLocalizedString(@"password", nil)];
    [self.textFieldConfirmPassword setPlaceholder:DPLocalizedString(@"confirmPassword", nil)];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    if ([self.sex isEqualToString:@"female"])
    {
        [self.buttonWomen isActive];
    }
    else
    {
        self.sex = @"male";
        [self.buttonMan isActive];
    }
}

- (void)startWorking
{
    [self performSegueWithIdentifier:NEXT_CONTROLLER_SEGUE_ID sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - verification TextField methods

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(BOOL)verificationOfFields
{
    if (![self validateEmailWithString:self.textFieldEmail.text])
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Введите корректный e-mail адрес"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
             if (buttonIndex == 1)
             {
                 // delete
             }
         }];
        return NO;
    }
    if (![self.textFieldPassword.text isEqualToString:self.textFieldConfirmPassword.text])
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Пароли  не совпадают!"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
             if (buttonIndex == 1)
             {
                 // delete
             }
         }];
        return NO;
    }
    if (self.textFieldPassword.text.length<6)
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Пароль должен содержать не менее 6 символов!"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
             if (buttonIndex == 1)
             {
                 // delete
             }
         }];
        return NO;
    }
    if (self.textFieldName.text.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Введите имя!"
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
             if (buttonIndex == 1)
             {
                 // delete
             }
         }];
        return NO;
    }
    
    return YES;
}

#pragma mark - pressed buttons methods

- (IBAction)pressedButtonRegistration:(id)sender
{
    if ([self verificationOfFields])
    {
        [self.delegate showProgressHUD];
        [WBAPIClient registrationWithFirstName:self.textFieldName.text
                                        gender:self.sex
                                         email:self.textFieldEmail.text
                                      password:self.textFieldPassword.text
                                      complete:^(id responseObject, NSError *error) {
            if (error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                NSLog(@"%@", error.userInfo[@"NSLocalizedDescriptionKey"]);
                [[[UIAlertView alloc] initWithTitle:@"Ошибка регистрации" message:@"Повторите действие позже."
                                  cancelButtonTitle:@"Закрыть"]
                 showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                     if (buttonIndex == 1)
                     {
                         // delete
                     }
                 }];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"Вы успешно зарегестрировались" message:@"Вам на email выслано письмо с подтверждением регистрации."
                                  cancelButtonTitle:@"Закрыть"]
                 showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                     if (buttonIndex == 0)
                     {
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                 }];
                [self.delegate hideProgressHUD];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
    }
}

- (IBAction)pressedButtonWomen:(id)sender
{
    [self.buttonWomen isActive];
    [self.buttonMan isNoActive];
    _sex = @"female";
}

- (IBAction)pressedButtonMan:(id)sender
{
    [self.buttonWomen isNoActive];
    [self.buttonMan isActive];
    _sex = @"male";
}

@end
