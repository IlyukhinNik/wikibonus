//
//  GiftButton.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/21/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "GiftButton.h"

@implementation GiftButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void) setHighlighted:(BOOL)highlighted
{
    if(highlighted) {
        self.backgroundColor = [UIColor colorWithRed:1 green:0.643 blue:0.282 alpha:1];
    } else {
        self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    }
    [super setHighlighted:highlighted];
}
@end
