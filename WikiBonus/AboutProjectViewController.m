//
//  AboutProjectViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "AboutProjectViewController.h"

@interface AboutProjectViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *_contactsArray;
}
@property (weak, nonatomic) IBOutlet UITableViewCell *tableViewContactsCell;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContacts;

@property (weak, nonatomic) IBOutlet UIButton *buttonBecomePartner;
@property (weak, nonatomic) IBOutlet UILabel *labelAboutProject;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogo;

- (IBAction)toBecomePartnerButtonPressed:(id)sender;

@end

@implementation AboutProjectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableViewContacts.layer.borderWidth = 0.5;
    self.tableViewContacts.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.title = DPLocalizedString(@"aboutProject", nil);
    _contactsArray = @[@"+38 075 321-65-98", @"partner@partner_website.ua", @"wikibonus_partner"];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(244, 110), NO, scaleScreen);
    [WikiBonusStyleKit drawLogoProjectIconWithFrame:CGRectMake(0, 0, 244, 110)];
    [self.imageViewLogo setImage:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    [self.labelAboutProject sizeToFit];
    [self.view layoutIfNeeded];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _contactsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell* cell = [self.tableViewContacts dequeueReusableCellWithIdentifier:WB_CONTACT_CELL];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil) {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:WB_CONTACT_CELL];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:13.0];
    cell.textLabel.text = [_contactsArray objectAtIndex:indexPath.row];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(25, 25), NO, scaleScreen);
    [WikiBonusStyleKit drawEmailContactWithFrame:CGRectMake(0, 0, 25, 25)];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cell;
}



#pragma mark button pressed methods

- (IBAction)toBecomePartnerButtonPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error", nil) message:DPLocalizedString(@"inDevelopment", nil)                      cancelButtonTitle:DPLocalizedString(@"close", nil)]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
     }];
}
@end
