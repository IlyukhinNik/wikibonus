//
//  Fragmen.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/18/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "Fragmen.h"

@implementation Fragmen

-(void)updateFragmentWithID:(NSInteger)idFragment nameFragment:(NSString*)nameFragment imageFragment:(UIImage*)imageFragment dateDead:(NSDate*)dateDead isReceived:(BOOL)isRecieved
{
    self.idFragment = idFragment;
    self.nameFragment = nameFragment;
    self.dateDead = dateDead;
    self.imageFragment = imageFragment;
    self.isReceived = isRecieved;
}


@end
