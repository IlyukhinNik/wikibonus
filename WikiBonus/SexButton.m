//
//  SexButton.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/30/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "SexButton.h"

@implementation SexButton


- (void)drawRect:(CGRect)rect
{
    [WikiBonusStyleKit drawGenderButtonWithFrame: CGRectMake(5, 15, 20, 20) pressed: self.isPressed];
}
-(void)setButtonTitleWithString:(NSString*)buttonTitle
{
    self.sexString = buttonTitle;
}
-(void)isNoActive
{
    self.isPressed = NO;
    [self setNeedsDisplay];
}
-(void)isActive
{
    self.isPressed = YES;
    [self setNeedsDisplay];
}

@end
