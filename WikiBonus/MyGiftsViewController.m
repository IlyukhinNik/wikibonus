//
//  MyGiftsViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "MyGiftsViewController.h"
#import "MyGiftTableViewCell.h"
#import "GiftViewController.h"
#import "Gift.h"
#import "Fragmen.h"

@interface MyGiftsViewController () <UITableViewDataSource, UITableViewDelegate>
{
    Gift *_currentGift;
    NSDateFormatter *_dateFormatter;
}

@end

@implementation MyGiftsViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    _dateFormatter = [NSDateFormatter new];
    [_dateFormatter setDateFormat:@"dd.MM.YYYY"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.giftsArray.count == 0)
    {
        UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,
                                                                        self.tableViewMyGifts.bounds.size.width,
                                                                        self.tableViewMyGifts.bounds.size.height)];
        messageLbl.textColor = [UIColor lightGrayColor];
        messageLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
        messageLbl.text = @"Список пуст";
        messageLbl.textAlignment = NSTextAlignmentCenter;
        [messageLbl sizeToFit];
        self.tableViewMyGifts.backgroundView = messageLbl;
    }
    else
        self.tableViewMyGifts.backgroundView = nil;
    
    return self.giftsArray.count;

    
    return self.giftsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 118;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MyGiftTableViewCell* cell = [self.tableViewMyGifts dequeueReusableCellWithIdentifier:@"cellGifts"];
    if (cell == nil)
    {
        cell = [[MyGiftTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellGifts"];
    }
    Gift *tmpGift = [self.giftsArray objectAtIndex:indexPath.row];
    cell.labelNameGift.text = tmpGift.nameGift;
    cell.imageViewPhotoGift.image = tmpGift.imageGift;
    cell.labelExpiredDate.text = [NSString stringWithFormat:@"Действительно до: %@", [_dateFormatter stringFromDate:tmpGift.dateDead]];
    if (tmpGift.procentFull > 0)
    {
        cell.labelProcentGift.text = [NSString stringWithFormat:@"%d%%", (int)ceilf(tmpGift.procentFull*100)];
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(88, 88), NO, scaleScreen);
        [WikiBonusStyleKit drawProcentGiftWithProcentFull:tmpGift.procentFull dem: 86 strokeWidth: 2];
        cell.imageViewProcentGift.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
    }    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showGift"])
    {
        NSIndexPath *path = [self.tableViewMyGifts indexPathForSelectedRow];
        _currentGift = [_giftsArray objectAtIndex:path.row];
        GiftViewController *giftVC = [segue destinationViewController];
        giftVC.gift = _currentGift;
        
    }
}

@end
