//
//  socNetInformation.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/11/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocNetInformation : NSObject

@property(nonatomic, strong) NSString *nameSocNet;
@property(nonatomic, strong) NSString *linkSocNet;
@property(nonatomic, strong) UIImage *imageSocNet;

-(void)updateSocNetInformationWithName:(NSString*)nameSocNet linkSocNet:(NSString*)linkSocNet imageSocNet:(UIImage*)imageSocNet;

@end
