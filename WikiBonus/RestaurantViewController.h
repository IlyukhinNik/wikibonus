//
//  RestaurantViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/19/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PoinOfSale.h"

@interface RestaurantViewController : UIViewController

@property (nonatomic) PointOfSale *pointsOfSale;

@end
