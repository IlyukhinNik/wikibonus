//
//  HallOfFameViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "HallOfFameViewController.h"
#import "HallOfFameTableViewCell.h"

@interface HallOfFameViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *_dictionaryUserInfo;
}

@end

@implementation HallOfFameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTestData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setTestData
{
    _dictionaryUserInfo = @{@"userPosition" : @"1",
                            @"userName": @"Иван Иванов",
                            @"userGiftsCount": @"302",
                            @"userFragmentsCount": @"1630"};
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0)
        return 3;
    
    else if (section == 1)
        return 3;
    
    else
        return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HallOfFameTableViewCell* cell = [self.tableViewHallOfFame dequeueReusableCellWithIdentifier:@"cellUser"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        cell = [[HallOfFameTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellUser"];
    }
    cell.labelPositionOfUser.text = [NSString stringWithFormat:@"%ld", indexPath.section*3+(long)indexPath.row+1];
    cell.labelNameOfUser.text = [_dictionaryUserInfo objectForKey:@"userName"];
    cell.imageViewPhotoUser.image = [UIImage imageNamed:@"gift.png"];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section !=0)
        return  30.0;
    return 0.0;
    
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return UIView.new;
}

@end
