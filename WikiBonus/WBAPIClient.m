//
//  WBAPIClient.m
//  WikiBonus
//
//  Created by куборубо on 04.11.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "WBAPIClient.h"
#import "WBError.h"
#import "WBParser.h"
#import "AutorizationViewController.h"

@implementation WBAPIClient

+ (AFHTTPSessionManager *)manager
{
    static AFHTTPSessionManager *_manager;
    if (!_manager)
    {
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:WB_BASE_URL]];
    }
    return _manager;
}

+ (void)startMonitoringInternetAvailability
{
    [[WBAPIClient manager].reachabilityManager startMonitoring];
}
+(void)deleteAccauntWithComplete:(void (^)(id responseObject, NSError *error))complete
{
    if (!complete)
    {
        
    }

    NSDictionary *parameters = @{@"api-key" : WB_AUTH_KEY,
                                 WB_SID : [[NSUserDefaults standardUserDefaults] objectForKey:WB_SID]};

    [[WBAPIClient manager] DELETE:WB_CLIENT parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
        complete(nil, error); // need to insert correct error message
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}
// ------------------------------Reachability------------------------------
+ (BOOL)isReachableInternet
{
    return [WBAPIClient manager].reachabilityManager.isReachable;
}
+ (void)authLoginWithEmail:(NSString*)email andPassword:(NSString*)password complete:(void (^)(id responseObject, NSError *error))complete {
    
    NSDictionary *parameters = @{@"api-key" : WB_AUTH_KEY,
                                 WB_AUTH_LOGIN : email,
                                 WB_AUTH_PASSWORD : password};
    
    [[WBAPIClient manager] GET:WB_AUTH_URL parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
           complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
    }];
}

+(void)registrationWithFirstName:(NSString*) firstName gender:(NSString*)genderUser email:(NSString*) email password:(NSString*) password
                        complete:(void (^)(id responseObject, NSError *error))complete {
    
    if (!complete)
    {
        
    }
    NSDictionary *parameters = @{@"api-key" : WB_AUTH_KEY,
                                 WB_AUTH_FIRST_NAME : firstName,
                                 WB_AUTH_GENDER : genderUser,
                                 WB_AUTH_EMAIL : email,
                                 WB_AUTH_PASSWORD : password};
    [[WBAPIClient manager] POST:WB_REG_URL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(nil, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
    }];
}



+(void)socNetRegistrationWithUser:(User*)socNetUser
                              complete:(void (^)(id responseObject, NSError *error))complete
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSDictionary *parameters = @{@"api-key" : WB_AUTH_KEY,
                                 WB_AUTH_FIRST_NAME : socNetUser.firstNameUser.length > 0 ? socNetUser.firstNameUser: @"",
                                 WB_AUTH_LAST_NAME : socNetUser.lastNameUser.length > 0 ? socNetUser.lastNameUser : @"",
                                 WB_AUTH_GENDER : socNetUser.gender.length > 0 ? socNetUser.gender : @"",
                                 WB_AUTH_EMAIL : socNetUser.emailUser.length > 0 ? socNetUser.emailUser : @"",
                                 WB_SOC_NET : socNetUser.socialNetUser.length > 0 ? socNetUser.socialNetUser : @"",
                                 WB_SOC_NET_URL: [NSString stringWithFormat:@"http://vk.com/id%@", socNetUser.userSocURL].length > 0 ? socNetUser.userSocURL : @"",
                                 WB_AUTH_CITY : socNetUser.cityUser.length > 0 ? socNetUser.cityUser : @"",
                                 WB_AUTH_BDATE : [dateFormatter stringFromDate:socNetUser.bdayUser].length > 0 ? socNetUser.bdayUser : @"",
                                 WB_AUTH_PHOTO_URL : socNetUser.photoURL.length > 0 ? socNetUser.photoURL :@""};
    [[WBAPIClient manager] POST:WB_REG_URL_SOC_NET parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
        NSLog(@"Error: %@", error.localizedDescription);
    }];
    
}

+(void)exitFromAccountWithComplete:(void (^)(id responseObject, NSError *error))complete
{
    if (!complete)
    {
        
    }
    NSDictionary *parameters = @{@"api-key" : WB_AUTH_KEY,
                                     WB_SID : [[NSUserDefaults standardUserDefaults] objectForKey:WB_SID]};
    
    [[WBAPIClient manager] DELETE:WB_AUTH_URL parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}

+(void)changePasswordWithOldPassword:(NSString*)passwordOld newPassword:(NSString*)passwordNew complete:(void (^)(id responseObject, NSError *error))complete
{
    if (!complete)
    {
        
    }
    NSDictionary *parameters = @{WB_AUTH_KEY
                                 WB_SID : [[NSUserDefaults standardUserDefaults] objectForKey:WB_SID],
                                 @"psw": passwordOld,
                                 @"psw_new": passwordNew};
    
    [[WBAPIClient manager] PUT:WB_CLIENT parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
    {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    }
    failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        complete(nil, error); // need to insert correct error message
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}

+(void)changeProfileWithParameters:(NSDictionary*)parameters complete:(void (^)(id responseObject, NSError *error))complete
{
    if (!complete)
    {
        
    }
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [param setObject:WB_AUTH_KEY forKey:@"api-key"];
    [param setObject:[[NSUserDefaults standardUserDefaults] objectForKey:WB_SID] forKey:@"SID"];
    
    [[WBAPIClient manager] POST:WB_CLIENT parameters:param success:^(NSURLSessionDataTask *task, id responseObject)
     {
         WBError *error = [WBParser errorFromResponseObject:responseObject];
         if(error.errorCode != 200)
         {
             complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
         }
         else
         {
             complete(responseObject, nil);
         }
         NSLog(@"%ld", (long)error.errorCode);
     }
                       failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         complete(nil, error); // need to insert correct error message
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}

+ (void)categoryPointsOfSaleListWithParams:(NSDictionary*)parameters
                  complete:(void (^)(id responseObject, NSError *error))complete {
    
    if (!complete)
    {
        
    }
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [param setObject:WB_AUTH_KEY forKey:@"api-key"];
    
        [[WBAPIClient manager] GET:WB_BUSINESS_CAT parameters:param success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
    }];
}

+ (void)pointsOfSaleListWithParams:(NSDictionary*)parameters
                                  complete:(void (^)(id responseObject, NSError *error))complete {
    
    if (!complete)
    {
        
    }
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [param setObject:WB_AUTH_KEY forKey:@"api-key"];
    [[WBAPIClient manager] GET:WB_POINTS_LIST parameters:param success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
    }];
}


+ (void)geoSearch:(NSDictionary*)parameters
                          complete:(void (^)(id responseObject, NSError *error))complete {
    
    if (!complete)
    {
        
    }
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [param setObject:WB_AUTH_KEY forKey:@"api-key"];
    [[WBAPIClient manager] GET:WB_BUSINESS_CAT parameters:param success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
    }];
}


+ (void)companyInfoWithParams:(NSDictionary*)parameters
                          complete:(void (^)(id responseObject, NSError *error))complete {
    
    if (!complete)
    {
        
    }
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [param setObject:WB_AUTH_KEY forKey:@"api-key"];
    [[WBAPIClient manager] GET:WB_COMPANY parameters:param success:^(NSURLSessionDataTask *task, id responseObject) {
        WBError *error = [WBParser errorFromResponseObject:responseObject];
        if(error.errorCode != 200)
        {
            complete(nil, [NSError errorWithDomain:@"WIKIBONUS" code:error.errorCode userInfo:[NSDictionary dictionaryWithObject:error.errorMessage forKey:NSLocalizedDescriptionKey]]);
        }
        else
        {
            complete(responseObject, nil);
        }
        NSLog(@"%ld", (long)error.errorCode);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(nil, error); // need to insert correct error message
    }];
}




@end
