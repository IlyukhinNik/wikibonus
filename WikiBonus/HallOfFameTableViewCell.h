//
//  HallOfFameTableViewCell.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HallOfFameTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelCoutGiftsUser;
@property (weak, nonatomic) IBOutlet UILabel *labelCountFragmentsUser;

@property (weak, nonatomic) IBOutlet UILabel *labelPositionOfUser;
@property (weak, nonatomic) IBOutlet UILabel *labelNameOfUser;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhotoUser;
@end
