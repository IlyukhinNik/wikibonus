//
//  Gift.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/11/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Gift : NSObject

@property (nonatomic) NSInteger idGift;
@property (nonatomic, strong) NSString *nameGift;
@property (nonatomic, strong) NSString *descriptionGift;
@property (nonatomic, strong) UIImage *imageGift;
@property (nonatomic) NSInteger likesCount;
@property (nonatomic) NSInteger reviewsCount;
@property (nonatomic) NSInteger idPointOfSale;
@property (nonatomic) NSDate *dateDead;
@property (nonatomic) NSArray *arrayFragments;
@property (nonatomic) BOOL inWishes;
@property (nonatomic) BOOL isReceived;
@property (nonatomic) float procentFull;

-(void)updateGiftWithIDGift:(NSInteger)idGift nameGift:(NSString*)nameGift description:(NSString*)description image:(UIImage*)image likesCount:(NSInteger)likesCount reviewsCount:(NSInteger)reviewsCount idPointOfSale:(NSInteger)idPointOfSale dateDead:(NSDate*)dateDead arrayFragments:(NSArray*)arrayFragments inWishes:(BOOL)inWishes isRecieved:(BOOL)isRecieved procentFull:(float)procentFull;
@end
