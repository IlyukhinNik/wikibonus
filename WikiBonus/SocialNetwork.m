//
//  SocialNetwork.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 2/26/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "SocialNetwork.h"


@interface SocialNetwork ()
{
    NSString* _currentSocNet;
}

@end

@implementation SocialNetwork

+ (SocialNetwork *)sharedInstance {
    static SocialNetwork *sharedInstance = nil;
    
    static dispatch_once_t onceToken; // onceToken = 0
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SocialNetwork alloc] init];
    });
    
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    return self;
}

// use "vk" - Vkontakte, "fb" - Facebook, g+ - Google+

-(BOOL)logInWithSocNet:(NSString*)socNet
{
    _currentSocNet = socNet;
    if ([socNet isEqualToString:@"vk"])
    {
        self.vkSocManager = [[VKManager alloc] init];
        return [self.vkSocManager logIn];;
    }
    else if ([socNet isEqualToString:@"fb"])
    {
        self.fbSocManager = [[FBManager alloc] init];
        return [self.fbSocManager logIn];
    }
    else
    {
        _currentSocNet = nil;
        NSLog(@"Error! Enter correct Social network!");
        return NO;
    }    
}


-(void)logOut
{
    if ([_currentSocNet isEqualToString:@"vk"])
        [self.vkSocManager logOut];
    else if ([_currentSocNet isEqualToString:@"fb"])
   [self.fbSocManager logOut];
}


#pragma mark - sharing with Social Network

// if socNet == nil , will be use current Social network ; use "vk" - Vkontakte, "fb" - Facebook, g+ - Google+
-(void)shareWithSocNet:(NSString*)socNet text:(NSString*)text imagesArray:(NSArray*)imagesArray URL:(NSURL*)url viewController:(UIViewController*)vc
{
    if (!socNet && _currentSocNet)
        socNet = _currentSocNet;
    else
        NSLog(@"Error! Enter correct Social network!");
    if ([socNet isEqualToString:@"vk"])
    {
        [self.vkSocManager shareVKText:text andImagesArray:imagesArray andUrl:url andVC:vc];
    }
    else if ([socNet isEqualToString:@"fb"])
    {
        [self.fbSocManager shareFBText:text andUrl:url andVC:vc];
    }
    else
    {
        _currentSocNet = nil;
        NSLog(@"Error! Enter correct Social network!");
    }
}

//activity controller for all soc net
-(void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url andVC:(UIViewController*)vc
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text)
    {
        [sharingItems addObject:text];
    }
    if (image)
    {
        [sharingItems addObject:image];
    }
    if (url)
    {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    [vc presentViewController:activityController animated:YES completion:nil];
}

#pragma mark - users data methods


-(NSDictionary*)getUserData
{
    if ([_currentSocNet isEqualToString:@"vk"])
        return [self.vkSocManager getUserData];
    else if ([_currentSocNet isEqualToString:@"fb"])
        return [self.vkSocManager getUserData];
    else
        return nil;
}


@end
