//
//  DXAnnotationSettings.m
//  CustomCallout
//
//  Created by Nikita Ilyukhin on 12/04/15.
//  Copyright (c) 2015 Nikita Ilyukhin. All rights reserved.
//

#import "PartnersPinAnnotationSettings.h"

@implementation PartnersPinAnnotationSettings

+ (instancetype)defaultSettings
{
    PartnersPinAnnotationSettings *newSettings = [[super alloc] init];
    if (newSettings)
    {
        newSettings.calloutOffset = 10.0f;

        newSettings.shouldRoundifyCallout = NO;
        newSettings.calloutCornerRadius = 5.0f;

        newSettings.shouldAddCalloutBorder = NO;
        newSettings.calloutBorderColor = [UIColor colorWithRed:0.329 green:0.565 blue:0.616 alpha:1.000];
        newSettings.calloutBorderWidth = 2.0;

        newSettings.animationType = DXCalloutAnimationZoomIn;
        newSettings.animationDuration = 0.2;
    }
    return newSettings;
}

@end
