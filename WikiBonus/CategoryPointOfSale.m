//
//  CategoryPointOfSale.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/25/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "CategoryPointOfSale.h"

@implementation CategoryPointOfSale

- (void)updatePointCategoryWithCategoryName:(NSString*)nameCategory idCategory:(NSInteger)idCategory imageСategory:(UIImage*)imageCategory
{
    self.nameCategory = nameCategory;
    self.idCategory = idCategory;
    self.imageCategory = imageCategory;
}


@end
