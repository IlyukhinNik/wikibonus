//
//  ChangePasswordView.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/27/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangePasswordViewDelegate <NSObject>
@required

- (void)hideChangePasswordView;

@end

@interface ChangePasswordView : UIView

@property (weak, nonatomic) id<ChangePasswordViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldNewPassword;
- (IBAction)pressedButtonChangePassword:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonChangePassword;

@end
