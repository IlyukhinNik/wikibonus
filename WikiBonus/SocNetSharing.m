//
//  SocNetSharing.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 1/28/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "SocNetSharing.h"

@implementation SocNetSharing

+(void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url andVC:(UIViewController*)vc
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text)
    {
        [sharingItems addObject:text];
    }
    if (image)
    {
        [sharingItems addObject:image];
    }
    if (url)
    {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    [vc presentViewController:activityController animated:YES completion:nil];
}

+(void)shareVKText:(NSString*)text andImagesArray:(NSArray*)imagesArray andUrl:(NSURL*)url andVC:(UIViewController*)vc
{
    VKShareDialogController *shareDialog = [VKShareDialogController new];
    shareDialog.text = text;
    shareDialog.uploadImages = imagesArray;
    shareDialog.shareLink = [[VKShareLink alloc] initWithTitle:@"" link:[NSURL URLWithString:@"https://vk.com/dev/ios_sdk"]];
    [shareDialog setCompletionHandler:^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
        [vc dismissViewControllerAnimated:YES completion:nil];
    }];
    [vc presentViewController:shareDialog animated:YES completion:nil];
}
@end
