//
//  FilterView.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/24/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "FilterView.h"
#import "CategoryPointOfSale.h"

@implementation FilterView

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.searchBarPointsOfSale.returnKeyType = UIReturnKeyDone;
    self.navigationBarFilter.topItem.title = DPLocalizedString(@"filter", nil);
    [self.searchBarPointsOfSale setPlaceholder:DPLocalizedString(@"searchByPartners", nil)];
    self.arrayCurrentCategories = [NSMutableArray arrayWithCapacity:self.arrayKindOfPartners.count];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayKindOfPartners.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryPointOfSale *currentCategoryPointOfSale = self.arrayKindOfPartners [indexPath.row];
    UITableViewCell* cell = [self.tableViewFilterObjects dequeueReusableCellWithIdentifier:@"FilterCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilterCell"];
    }
    cell.textLabel.text = currentCategoryPointOfSale.nameCategory;
    cell.imageView.image = currentCategoryPointOfSale.imageCategory;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchBarPointsOfSale resignFirstResponder];
    UITableViewCell *cell = [self.tableViewFilterObjects cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        NSMutableArray *removeElements = [@[] mutableCopy];
        for(CategoryPointOfSale *curCategoryPointOfSale in self.arrayCurrentCategories)
            if ([self.arrayKindOfPartners objectAtIndex:indexPath.row] == curCategoryPointOfSale)
                [removeElements addObject:curCategoryPointOfSale];
        
        [self.arrayCurrentCategories removeObjectsInArray:removeElements];
        
//        for (NSInteger i = 0; i < self.arrayCurrentCategories.count; i++)
//        {
//            if (self.arrayCurrentCategories [i] == self.arrayKindOfPartners [indexPath.row])
//            {
//                [self.arrayCurrentCategories removeObjectAtIndex:i];
//                i--;
//            }
//        }
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.arrayCurrentCategories addObject:[self.arrayKindOfPartners objectAtIndex:indexPath.row]];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
     ((UITableViewHeaderFooterView *)view).backgroundView.backgroundColor = [UIColor clearColor];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
//    NSString *sectionName = @"Выбрать по категории";
    return DPLocalizedString(@"selectByCategory", nil);
}

#pragma mark - Pressed Button methods

- (IBAction)cancelButtonPressed:(id)sender
{
    [_searchBarPointsOfSale endEditing:YES];
    [self.delegate hideFilterViewAndUpdatePointsWithChoosenCategoriesArray:self.arrayCurrentCategories andFilterString:self.searchBarPointsOfSale.text];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
@end
