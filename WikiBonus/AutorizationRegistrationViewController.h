//
//  AutorizationRegistrationViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/30/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutorizationRegistrationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlAutorization;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containtViewHeight;

@end
