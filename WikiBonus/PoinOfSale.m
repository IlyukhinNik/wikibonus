//
//  Partner.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/17/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "PoinOfSale.h"

@implementation PointOfSale

- (void)updatePointOfSaleForMapViewWithID:(NSInteger)idPointOfSale
                                idCompany:(NSInteger)idCompany
                          namePointOfSale:(NSString*)pointOfSaleName
                       pointOfSaleAddress:(AddressPointOfSale*)pointOfSaleAddress
                    coordinatePointOfSale:(CLLocationCoordinate2D)
coordinatePointOfSale category:(CategoryPointOfSale*)categoryPointOfSale
                                starsRate:(float)starsRate aboutCompanyString:(NSString*)aboutCompany
{
    self.idPointOfSale = idPointOfSale;
    self.idCompany = idCompany;
    self.namePointOfSale = pointOfSaleName;
    self.addressPointOfSale = pointOfSaleAddress;
    self.coordinatesPointOfSale = coordinatePointOfSale;
    self.starsCount =starsRate;
    self.aboutCompany = aboutCompany;
    self.categotyPointOfSale = categoryPointOfSale;
}

-(void)updatePointWithMissingInfoLogoImage:(UIImage *)imageLogo description:(NSString *)description contacts:(NSDictionary *)contactsDictionary socialURLArray:(NSMutableArray *)socialURLArray
{
    self.imageLogoPointOfSale = imageLogo;
    self.descriptionCompany = description;
    self.contactsDictionary =contactsDictionary;
    self.socialArray = socialURLArray;
}

-(void)updatePointWithCompanyInfoDictionary:(NSDictionary*)companyInfo
{
    NSDictionary *dictionaryContactsInformation = @{@"address" :companyInfo[@"address"],
                                                    @"phone" : @"+79097777777",
                                                    @"email": @"mail@mail.com",
                                                    @"mphone": @"+79097777777"};

    self.contactsDictionary =  dictionaryContactsInformation;
    self.imageLogoPointOfSale = companyInfo[@"logo_img"];
    self.descriptionCompany = companyInfo[@"descr"];
}
@end
