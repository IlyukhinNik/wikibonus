//
//  WBError.m
//  WikiBonus
//
//  Created by куборубо on 04.11.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "WBError.h"

@implementation WBError

-(instancetype)initWithErrorCode:(NSInteger) errorCode andErrorMessage:(NSString*)errorMessage
{
    self = [super init];
    if(self)
    {
        self.errorCode = errorCode;
        self.errorMessage = errorMessage;
    }
    return self;
}
@end
