//
//  ReplaceSegue.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/1/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "ReplaceSegue.h"

@implementation ReplaceSegue

- (void)perform
{
    UIViewController *source = self.sourceViewController;
    UIViewController *destination = self.destinationViewController;
    UIViewController *container = source.parentViewController;
    
    [container addChildViewController:destination];
    destination.view.frame = source.view.frame;
    [source willMoveToParentViewController:nil];
    
    [container transitionFromViewController:source
                           toViewController:destination
                                   duration:0.5
                                    options:UIViewAnimationOptionTransitionCrossDissolve
                                 animations:^{
                                 }
                                 completion:^(BOOL finished) {
                                     [source removeFromParentViewController];
                                     [destination didMoveToParentViewController:container];
                                 }];
}

@end
