//
//  GotGiftQRViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 2/8/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "GotGiftQRViewController.h"

#import "RSCodeView.h"
#import "RSCodeGen.h"
#import <RSCodeGenerator.h>
@interface GotGiftQRViewController ()
@property (weak, nonatomic) IBOutlet RSCodeView *QRGenCodeView;

@end

@implementation GotGiftQRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.QRGenCodeView.code = resizeImage([[RSUnifiedCodeGenerator codeGen]  genCodeWithContents:@"https://vk.com" machineReadableCodeObjectType:AVMetadataObjectTypeQRCode], 10);
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
