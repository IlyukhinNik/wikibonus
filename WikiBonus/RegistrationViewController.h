//
//  RegistrationViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SexButton.h"


@protocol RegistrationViewControllerDelegate <NSObject>

-(void)showProgressHUD;
-(void)hideProgressHUD;

@end

@interface RegistrationViewController : UIViewController

@property (weak, nonatomic) id<RegistrationViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *textFieldRegistration;
@property (weak, nonatomic) IBOutlet UIButton *buttonRegistrate;
@property (weak, nonatomic) IBOutlet SexButton *buttonMan;
@property (weak, nonatomic) IBOutlet SexButton *buttonWomen;
@property (strong, nonatomic) NSString *sex;

@end
