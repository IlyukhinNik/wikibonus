//
//  ProfileViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/9/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController
{
    IBOutlet TPKeyboardAvoidingScrollView *scroler;
    UIDatePicker* datePicker;
}
@end
