//
//  MKAnnotationCustom.m
//  MapExample
//
//  Created by Harlan Kellaway on 11/5/14.
//  Copyright (c) 2014 ___HARLANKELLAWAY___. All rights reserved.
//

#import "MKAnnotationCustom.h"


@implementation MKAnnotationCustom

- (id)initWithPointOfSale:(PointOfSale *)pointOfSale
{
    self = [super init];
    
    if (self)
    {
        if ([pointOfSale.namePointOfSale isKindOfClass:[NSString class]])
        {
            _title = pointOfSale.namePointOfSale;
        }
        else
        {
            _title = @"Имя не указано";
        }
        
        _subtitle = [pointOfSale.addressPointOfSale addressToString];
        _coordinate = pointOfSale.coordinatesPointOfSale;
        _procent = 0.75;
        _starRate = pointOfSale.starsCount;
        _pointOfSale = pointOfSale;
    }
    return self;
}

@end
