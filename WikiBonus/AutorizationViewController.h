//
//  AutorizationViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 10/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"

@protocol AutorizationViewControllerDelegate <NSObject>

-(void)showRegViewControllerWithParamName:(NSString*)name gender:(NSString*)gender;
-(void)showProgressHUD;
-(void)hideProgressHUD;

@end

@interface AutorizationViewController : UIViewController <VKSdkDelegate>
@property (weak, nonatomic) id<AutorizationViewControllerDelegate> delegate;
@end
