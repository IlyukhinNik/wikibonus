//
//  TableViewCellWithButtons.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/23/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "TableViewCellWithButtons.h"

@implementation TableViewCellWithButtons

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
