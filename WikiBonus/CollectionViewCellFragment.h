//
//  CollectionViewCellFragment.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/22/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCellFragment : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewFragment;
@property (weak, nonatomic) IBOutlet UILabel *labelDateDead;

@end
