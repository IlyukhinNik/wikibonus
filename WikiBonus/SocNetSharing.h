//
//  SocNetSharing.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 1/28/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocNetSharing : NSObject

+(void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url andVC:(UIViewController*)vc;
+(void)shareVKText:(NSString*)text andImagesArray:(NSArray*)imagesArray andUrl:(NSURL*)url andVC:(UIViewController*)vc;

@end
