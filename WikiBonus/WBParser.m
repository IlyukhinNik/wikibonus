//
//  WBParser.m
//  WikiBonus
//
//  Created by куборубо on 04.11.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "WBParser.h"

@implementation WBParser

+(WBError*) errorFromResponseObject: (NSDictionary*) response
{
    NSInteger code = [response[@"code"] integerValue];
    NSString *message = response[@"message"];
    WBError *error = [[WBError alloc] initWithErrorCode:code andErrorMessage:message];
    return error;
}

+(User*) getUserFromDictionaryVK: (NSDictionary*) response
{
    User *user = [User new];
    user.firstNameUser = response[@"first_name"];
    user.lastNameUser = response[@"last_name"];
    if ([response[@"sex"] integerValue] == 2)
    {
        user.gender = @"male";
    }
    else
    {
        user.gender = @"female";
    }
    user.emailUser = response[@"email"];
    user.socialNetUser = @"vk";
    user.cityUser = [response[@"city"] objectForKey:@"title"];
    
    NSString *dateString = response[@"bdate"];
    NSDateFormatter *format = [NSDateFormatter new];
    [format setDateFormat:@"dd.MM.yyyy"];
    user.bdayUser = [format dateFromString:dateString];
    user.photoURL = response[@"photo_max_orig"];
    user.userSocURL = response[@"id"];
    return user;
}

+(User*) getUserFromDictionaryFB: (NSDictionary*) response
{
    User *user = [User new];
    user.firstNameUser = response[@"first_name"];
    user.lastNameUser = response[@"last_name"];
    user.gender = response[@"gender"];
    user.emailUser = response[@"email"];
    user.socialNetUser = @"fb";
    user.userSocURL= response[@"id"];
    user.cityUser = [response[@"location"] objectForKey:@"name"];
    
    NSString *dateString = response[@"birthday"];
    NSDateFormatter *format = [NSDateFormatter new];
    [format setDateFormat:@"MM/dd/yyyy"];
    user.bdayUser = [format dateFromString:dateString];
    user.photoURL = [[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", user.userSocURL ]] absoluteString];
    user.userSocURL = response[@"link"];
    return user;
}

+(User*) getUser: (NSDictionary*) response andEmail:(NSString*)email
{
    NSDictionary* dict = [[response objectForKey:@"profile"] objectForKey:@"data"];
    if (dict)
    {
        User *user = [User new];
        user.firstNameUser = [dict stringForKey:@"first_name"];
        user.lastNameUser = [dict stringForKey:@"last_name"];
        user.gender = [dict stringForKey:@"sex"];
        user.emailUser = email;
        //    user.cityUser = [[response objectForKey:@"city"] objectForKey:@"city_name"];
        NSString *dateString = [dict stringForKey:@"birthday"];
        NSDateFormatter *format = [NSDateFormatter new];
        [format setDateFormat:@"yyyy-MM-dd"];
        user.bdayUser = [format dateFromString:dateString];
        user.photoURL = [dict stringForKey:@"avatar"];
        user.userSID = [response stringForKey:@"SID"];
        return user;
    }
    return [User new];
}

+(User*) getUserFromPlistDictionary: (NSDictionary*) userInfoDictionary
{
    User *user = [User new];
    user.firstNameUser = [userInfoDictionary stringForKey:@"first_name"];
    user.lastNameUser = [userInfoDictionary stringForKey:@"last_name"];
    user.gender = [userInfoDictionary stringForKey:@"sex"];
    user.emailUser = [userInfoDictionary stringForKey:@"email"];
    user.phoneUser = [userInfoDictionary stringForKey:@"phone_user"];
    NSString *dateString = [userInfoDictionary stringForKey:@"birthday"];
    NSDateFormatter *format = [NSDateFormatter new];
    [format setDateFormat:@"yyyy-MM-dd"];
    user.bdayUser = [format dateFromString:dateString];
    user.photoURL = [userInfoDictionary stringForKey:@"avatar"];
    user.photoLocalURL = [userInfoDictionary stringForKey:@"avatar_local"];
    user.userSID = [userInfoDictionary stringForKey:@"SID"];
    user.cityUser = [userInfoDictionary stringForKey:@"city_name"];
    user.phoneUser = [userInfoDictionary stringForKey:@"phone_user"];
    return user;
}

+(NSDictionary*) makeDictionaryForPList: (NSDictionary*) response andEmail:(NSString*)email
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:[[response objectForKey:@"profile"] objectForKey:@"data"]];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
    NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    
    NSMutableDictionary* responseDictionary = [NSMutableDictionary dictionaryWithDictionary:resultDictionary];
    [responseDictionary addEntriesFromDictionary:dict];
    [responseDictionary setObject:[response objectForKey:@"SID"] forKey:@"SID"];
    [responseDictionary removeObjectsForKeys:[[NSArray alloc] initWithObjects:@"user_type", @"city", @"lang", nil ]];
    const id nul = [NSNull null];
    
    for(NSString *key in [responseDictionary allKeys])
    {
        const id object = [responseDictionary objectForKey:key];
        if(object == nul)
        {
            [responseDictionary setValue:@"" forKey:key];
        }
    }
    return responseDictionary;
}

+(PointOfSale*) getPointOfSale: (NSDictionary*) response
{
    NSDictionary* pointOfSaleInfoDictionary = [[response objectForKey:@"point"] objectForKey:@"data"];
    if (pointOfSaleInfoDictionary)
    {
        PointOfSale *parsedPointOfSale = [PointOfSale new];
        AddressPointOfSale *parsedAddressPointOfSale = [AddressPointOfSale new];
        
        parsedAddressPointOfSale.countryID = [pointOfSaleInfoDictionary integerForKey:@"country_id"];
        parsedAddressPointOfSale.cityID = [pointOfSaleInfoDictionary integerForKey:@"city_id"];
        parsedAddressPointOfSale.cityName = [pointOfSaleInfoDictionary stringForKey:@"city_name"];
        parsedAddressPointOfSale.address = [pointOfSaleInfoDictionary stringForKey:@"address"];
        
        parsedPointOfSale.idPointOfSale = [pointOfSaleInfoDictionary integerForKey:@"id"];
        parsedPointOfSale.idCompany = [pointOfSaleInfoDictionary integerForKey:@"company_id"];
        parsedPointOfSale.namePointOfSale = [pointOfSaleInfoDictionary stringForKey:@"name"];
        parsedPointOfSale.addressPointOfSale = parsedAddressPointOfSale;
        parsedPointOfSale.coordinatesPointOfSale = CLLocationCoordinate2DMake([pointOfSaleInfoDictionary integerForKey:@"geo_lat"], [pointOfSaleInfoDictionary integerForKey:@"geo_lng"]);
        parsedPointOfSale.categotyPointOfSale = [CategoryPointOfSale new];
        parsedPointOfSale.starsCount = ((float)rand() / RAND_MAX) * 5;
        parsedPointOfSale.aboutCompany = [NSString new];
        
        return parsedPointOfSale;
    }

    return [PointOfSale new];
}

+(NSMutableArray*) getArrayPointsOfSale: (NSDictionary*) response
{
    NSMutableArray *arrayParsedPointsOfSale = [NSMutableArray new];
    
    NSDictionary* infoDictionary = [[response objectForKey:@"point"] objectForKey:@"data"];
    for (NSDictionary* pointOfSaleInfoDictionary in infoDictionary)
    {
        PointOfSale *parsedPointOfSale = [PointOfSale new];
        AddressPointOfSale *parsedAddressPointOfSale = [AddressPointOfSale new];
        
        parsedAddressPointOfSale.countryID = [pointOfSaleInfoDictionary integerForKey:@"country_id"];
        parsedAddressPointOfSale.cityID = [pointOfSaleInfoDictionary integerForKey:@"city_id"];
        parsedAddressPointOfSale.cityName = [pointOfSaleInfoDictionary stringForKey:@"city_name"];
        parsedAddressPointOfSale.address = [pointOfSaleInfoDictionary stringForKey:@"address"];
        
        parsedPointOfSale.idPointOfSale = [pointOfSaleInfoDictionary integerForKey:@"id"];
        parsedPointOfSale.idCompany = [pointOfSaleInfoDictionary integerForKey:@"company_id"];
        parsedPointOfSale.namePointOfSale = [pointOfSaleInfoDictionary stringForKey:@"name"];
        parsedPointOfSale.addressPointOfSale = parsedAddressPointOfSale;
        parsedPointOfSale.coordinatesPointOfSale = CLLocationCoordinate2DMake([pointOfSaleInfoDictionary[@"geo_lat"] doubleValue], [pointOfSaleInfoDictionary[@"geo_lng"] doubleValue]);
        parsedPointOfSale.categotyPointOfSale = [CategoryPointOfSale new];
        parsedPointOfSale.starsCount = ((float)rand() / RAND_MAX) * 5;
        parsedPointOfSale.aboutCompany = [NSString new];
        
        [arrayParsedPointsOfSale addObject:parsedPointOfSale];
    }
    
    return arrayParsedPointsOfSale;
}
 
@end
