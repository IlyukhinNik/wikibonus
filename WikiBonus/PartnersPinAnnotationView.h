//
//  DXAnnotationView.h
//  CustomCallout
//
//  Created by Nikita Ilyukhin on 05/04/15.
//  Copyright (c) 2015 Nikita Ilyukhin. All rights reserved.
//

#import <MapKit/MapKit.h>
@class PartnersPinAnnotationSettings;

@interface PartnersPinAnnotationView : MKAnnotationView

@property(nonatomic, strong) UIView *pinView;
@property(nonatomic, strong) UIView *calloutView;

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation
                   reuseIdentifier:(NSString *)reuseIdentifier
                           pinView:(UIView *)pinView
                       calloutView:(UIView *)calloutView
                          settings:(PartnersPinAnnotationSettings *)settings;

- (void)hideCalloutView;
- (void)showCalloutView;

@end
