//
//  ImgavieBackgroundBorder.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "ImageViewBackgroundBorder.h"

@implementation ImageViewBackgroundBorder

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self setBorderWithWidth:0.5f];
    }
    return self;
}


-(void)setBorderWithWidth:(float)borderWidth
{
    self.layer.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    self.layer.borderWidth = borderWidth;
}
@end
