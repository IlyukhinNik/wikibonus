//
//  CollectionViewCellGift.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/11/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCellGift : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewGift;
@property (weak, nonatomic) IBOutlet UILabel *labelNameGift;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProcentGift;
@property (weak, nonatomic) IBOutlet UILabel *labelProcentGift;

@end
