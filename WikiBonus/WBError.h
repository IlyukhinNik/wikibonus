//
//  WBError.h
//  WikiBonus
//
//  Created by куборубо on 04.11.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WBError : NSObject

@property(nonatomic, strong) NSString *errorMessage;
@property(nonatomic) NSInteger errorCode;

-(instancetype)initWithErrorCode:(NSInteger) errorCode andErrorMessage:(NSString*)errorMessage;
@end
