//
//  MKAnnotationCustom.h
//  MapExample
//
//  Created by Harlan Kellaway on 11/5/14.
//  Copyright (c) 2014 ___HARLANKELLAWAY___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "PoinOfSale.h"

@interface MKAnnotationCustom : NSObject <MKAnnotation>


// MKAnnotation properties
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic, readonly) float procent;
@property (nonatomic, readonly) float starRate;
@property (nonatomic, readonly) PointOfSale *pointOfSale;

- (id)initWithPointOfSale:(PointOfSale*)pointOfSale;
@end
