//
//  MyGiftsViewController.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGiftsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewMyGifts;
@property (nonatomic) NSMutableArray *giftsArray;
@end
