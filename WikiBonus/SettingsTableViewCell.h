//
//  SettingsTableViewCell.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/15/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageSettings;
@property (weak, nonatomic) IBOutlet UILabel *labelSettings;
@end
