//
//  PartnersMapViewController.m
//  WikiBonus
//
//  Created by куборубо on 05.11.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <FBAnnotationClustering/FBAnnotationClustering.h>
#import "PartnersMapViewController.h"
#import "PartnersPinAnnotationView.h"
#import "PartnersPinAnnotationSettings.h"
#import "PopUpView.h"
#import "FilterView.h"

#import "Fragmen.h"
#import "Gift.h"

#import "WBAPIClient.h"
#import "WBParser.h"

#import "AutorizationViewController.h"
#import "MKAnnotationCustom.h"
#import "PoinOfSale.h"
#import "CategoryPointOfSale.h"
#import "FilterView.h"
#import "RestaurantViewController.h"
#import "MyGiftsViewController.h"
#import "UIButton+VerticalLayout.h"
#import <RSBarcodes/RSBarcodes.h>
#import "SocNetSharing.h"


@interface PartnersMapViewController () <MKMapViewDelegate, FBClusteringManagerDelegate, CLLocationManagerDelegate, FilterViewDelegate, PopUpViewDelegate>
{
    BOOL _isShowedFilter;
    BOOL _isShowedGiftsList;
    FilterView *_filterView;
    IBOutlet NSLayoutConstraint *menuButtonsYPosition;
    MyGiftsViewController *_giftViewCatalog;
    NSMutableArray *_testFragmentArray;
    NSMutableArray *_testGiftsArray;
    RSScannerViewController *_scanner;
}

@property (strong, nonatomic) IBOutlet UILabel *labelFiltresNames;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonList;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonFilter;

@property (strong, nonatomic) IBOutlet UIButton *buttonPersonalCabinet;
@property (strong, nonatomic) IBOutlet UIButton *buttonTrash;
@property (strong, nonatomic) IBOutlet UIButton *buttonScanQR;
@property (strong, nonatomic) IBOutlet UIButton *buttonSettings;

@property (nonatomic, strong) FBClusteringManager *clusteringManager;
@property (strong, nonatomic) IBOutlet MKMapView *mapViewPartners;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) UIImage *imageViewPin;

@property (strong, nonatomic) PopUpView *calloutView;

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *filterInfoHeight;

@property (nonatomic) NSMutableArray *testCoordinates;
@property (nonatomic) NSMutableArray *testCoordinatesFiltred;
@property (nonatomic) NSMutableArray *testGiftsFiltred;
@property (nonatomic) NSMutableArray *arrayAllCategories;
@property (nonatomic) NSMutableArray *arrayAcceptedCategories;

@property (nonatomic) NSMutableArray *arrayAllPointsOfSale;


- (IBAction)buttonTrashPressed:(id)sender;
- (IBAction)buttonGiftsListPressed:(id)sender;
- (IBAction)buttonQRCodePressed:(id)sender;

@end

@implementation PartnersMapViewController

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = DPLocalizedString(@"partnersMap", nil);
    [self setStatusBarAndNavigationController];
    [self setFilterView];
    [self setButtonsGraphics];
    [self getAllPointsOfSale];
    [self setMapView];
    
//    [self createTestPointsOfSale];
    [self setTestFragments];
    [self setTestGifts];
    [self setScaner];
    
    _giftViewCatalog = [self.childViewControllers firstObject];
    _giftViewCatalog.giftsArray = _testGiftsArray;
    [self.view bringSubviewToFront:self.mapViewPartners];
    NSLog(@"%f", [self getRadius]);
    
    [VKSdk initializeWithAppId:VK_APP_KEY];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - prepare to work methods
-(void)setScaner
{
    _scanner = [[RSScannerViewController alloc] initWithCornerView:YES
                                                      controlView:NO
                                                  barcodesHandler:^(NSArray *barcodeObjects) {
                                                      if (barcodeObjects.count > 0)
                                                      {
                                                          [barcodeObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  AVMetadataMachineReadableCodeObject *code = obj;
                                                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Найден QR  код"
                                                                                                                  message:code.stringValue
                                                                                                                 delegate:self
                                                                                                        cancelButtonTitle:@"OK"
                                                                                                        otherButtonTitles:nil];
                                                                  dispatch_async(dispatch_get_main_queue(), ^{
//                                                                      [_scanner dismissViewControllerAnimated:true completion:nil];
                                                                      [_scanner.navigationController popViewControllerAnimated:YES];
                                                                      [alert show];
                                                                      [_scanner stopRunning];
                                                                  });
                                                              });
                                                          }];
                                                      }
                                                      
                                                  }
               
                                          preferredCameraPosition:AVCaptureDevicePositionBack];
    _scanner.title = DPLocalizedString(@"scanner", nil);
    [_scanner setStopOnFirst:YES];
}

- (void)backButtonPressed
{
    // write your code to prepare popview
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setStatusBarAndNavigationController
{
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, -20, self.view.bounds.size.width, 20)];
    statusBarView.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:statusBarView];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

-(void)setFilterView
{
    self.filterInfoHeight.constant = 0.0;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _filterView = [mainStoryboard instantiateViewControllerWithIdentifier:@"FilterView"];
    _filterView.delegate = self;
    _isShowedFilter = NO;
    [self addChildViewController:_filterView];
    
    self.arrayAcceptedCategories = [NSMutableArray array];
    self.arrayAllCategories = [NSMutableArray array];
    NSArray *titleArray = [[NSArray alloc] initWithObjects:@"Пекарни" ,@"Рестораны",@"Кафе" ,@"Бары" ,@"Парикмахерские" ,@"Торговые центры" ,@"Тренажерные залы"  ,nil];
    NSMutableArray *imagesArray = [NSMutableArray new];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
    [WikiBonusStyleKit drawRestaurantWithFrame:CGRectMake(0, 0, 50, 50)];
    [imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    [imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    [imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    [imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
    [WikiBonusStyleKit drawHairWithFrame:CGRectMake(0, 0, 50, 50)];
    [imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
    [WikiBonusStyleKit drawBuldingFilterWithFrame:CGRectMake(0, 0, 50, 50)];
    [imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
    [WikiBonusStyleKit drawGymWithFrame:CGRectMake(0, 0, 50, 50)];
    [imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    _filterView.arrayKindOfPartners = titleArray;
    for (NSUInteger i = 0; i< titleArray.count; i++)
    {
        CategoryPointOfSale *currentCategory = [CategoryPointOfSale new];
        [currentCategory updatePointCategoryWithCategoryName:[titleArray objectAtIndex:i] idCategory:i imageСategory:[imagesArray objectAtIndex:i]];
        [self.arrayAllCategories addObject:currentCategory];
    }
    _filterView.arrayKindOfPartners =  self.arrayAllCategories;
}

-(void)setMapView
{
    self.calloutView.delegate = self;
    self.mapViewPartners.delegate = self;
    self.mapViewPartners.mapType = MKMapTypeStandard;
    self.mapViewPartners.showsUserLocation = YES;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

-(void)createTestPointsOfSale
{
    self.testCoordinates = [NSMutableArray array];
    self.testCoordinatesFiltred = [NSMutableArray array];
    
    PointOfSale* pointOfSale1 = [PointOfSale new];
    PointOfSale* pointOfSale2 = [PointOfSale new];
    PointOfSale* pointOfSale3 = [PointOfSale new];
    PointOfSale* pointOfSale4 = [PointOfSale new];
    PointOfSale* pointOfSale5 = [PointOfSale new];
    PointOfSale* pointOfSale6 = [PointOfSale new];
    PointOfSale* pointOfSale7 = [PointOfSale new];
    PointOfSale* pointOfSale8 = [PointOfSale new];
    PointOfSale* pointOfSale9 = [PointOfSale new];
    PointOfSale* pointOfSale10 = [PointOfSale new];
    PointOfSale* pointOfSale11 = [PointOfSale new];
    PointOfSale* pointOfSale12 = [PointOfSale new];
    
    // latitude = 48.445381119999979, longitude = 35.059971849999982 - Dnepr
    // latitude = 59.8944444, longitude = 30.2641667 - Piter
    // latitude = 55.7522222, longitude = 37.6155556 - Moscow
    
        CategoryPointOfSale *categoryPointOfSale1 = [CategoryPointOfSale new];
        [categoryPointOfSale1 updatePointCategoryWithCategoryName:@"Кафе" idCategory:2 imageСategory:nil];
        CategoryPointOfSale *categoryPointOfSale2 = [CategoryPointOfSale new];
        [categoryPointOfSale2 updatePointCategoryWithCategoryName:@"Пекарни" idCategory:0 imageСategory:nil];
        CategoryPointOfSale *categoryPointOfSale3 = [CategoryPointOfSale new];
        [categoryPointOfSale3 updatePointCategoryWithCategoryName:@"Бары" idCategory:3 imageСategory:nil];
        
        AddressPointOfSale *addressPointOfSale1 = [AddressPointOfSale new];
        [addressPointOfSale1 updateAddressPointOfSaleWhithCountryID:1 cityID:1 cityName:@"Днепропетровск" address:@"ул.Ванильная, 23/2"];
        AddressPointOfSale *addressPointOfSale2 = [AddressPointOfSale new];
        [addressPointOfSale2 updateAddressPointOfSaleWhithCountryID:1 cityID:1 cityName:@"Днепропетровск" address:@"ул.Ванильная, 28/7"];
        AddressPointOfSale *addressPointOfSale3 = [AddressPointOfSale new];
        [addressPointOfSale3 updateAddressPointOfSaleWhithCountryID:1 cityID:1 cityName:@"Днепропетровск" address:@"ул.Ванильная, 25/7"];
        AddressPointOfSale *addressPointOfSale4 = [AddressPointOfSale new];
        [addressPointOfSale4 updateAddressPointOfSaleWhithCountryID:1 cityID:1 cityName:@"Днепропетровск" address:@"ул.Ванильная, 2/7"];
        
        AddressPointOfSale *addressPointOfSale5 = [AddressPointOfSale new];
        [addressPointOfSale5 updateAddressPointOfSaleWhithCountryID:2 cityID:2 cityName:@"Москва" address:@"ул.Соленая, 2/22"];
        AddressPointOfSale *addressPointOfSale6 = [AddressPointOfSale new];
        [addressPointOfSale6 updateAddressPointOfSaleWhithCountryID:2 cityID:2 cityName:@"Москва" address:@"ул.Соленая, 1/74"];
        AddressPointOfSale *addressPointOfSale7 = [AddressPointOfSale new];
        [addressPointOfSale7 updateAddressPointOfSaleWhithCountryID:2 cityID:2 cityName:@"Москва" address:@"ул.Соленая, 5/178"];
        AddressPointOfSale *addressPointOfSale8 = [AddressPointOfSale new];
        [addressPointOfSale8 updateAddressPointOfSaleWhithCountryID:2 cityID:2 cityName:@"Москва" address:@"ул.Соленая, 102/323"];
        
        AddressPointOfSale *addressPointOfSale9 = [AddressPointOfSale new];
        [addressPointOfSale9 updateAddressPointOfSaleWhithCountryID:2 cityID:3 cityName:@"Санкт-Питербург" address:@"ул.Сладкая, 1/122"];
        AddressPointOfSale *addressPointOfSale10 = [AddressPointOfSale new];
        [addressPointOfSale10 updateAddressPointOfSaleWhithCountryID:2 cityID:3 cityName:@"Санкт-Питербург" address:@"ул.Сладкая, 89/174"];
        AddressPointOfSale *addressPointOfSale11 = [AddressPointOfSale new];
        [addressPointOfSale11 updateAddressPointOfSaleWhithCountryID:2 cityID:3 cityName:@"Санкт-Питербург" address:@"ул.Сладкая, 51/178"];
        AddressPointOfSale *addressPointOfSale12 = [AddressPointOfSale new];
        [addressPointOfSale12 updateAddressPointOfSaleWhithCountryID:2 cityID:3 cityName:@"Санкт-Питербург" address:@"ул.Сладкая, 12/33"];
    
    //Dnepr
    
        [pointOfSale1 updatePointOfSaleForMapViewWithID:1 idCompany:1 namePointOfSale:@"Кафе \"Карамель\"" pointOfSaleAddress:addressPointOfSale1 coordinatePointOfSale:CLLocationCoordinate2DMake(48.50543532, 35.05997) category:categoryPointOfSale1 starsRate:2.3 aboutCompanyString:@"О Карамеле"];
        [self.testCoordinates addObject:pointOfSale1];
        [pointOfSale2 updatePointOfSaleForMapViewWithID:2 idCompany:2 namePointOfSale:@"Пекарня \"Печенька\"" pointOfSaleAddress:addressPointOfSale2 coordinatePointOfSale:CLLocationCoordinate2DMake(48.54543532, 35.08997) category:categoryPointOfSale2 starsRate:4.0 aboutCompanyString:@"О Печеньке"];
        [self.testCoordinates addObject:pointOfSale2];
        [pointOfSale3 updatePointOfSaleForMapViewWithID:3 idCompany:1 namePointOfSale:@"Кафе \"Мотылек\"" pointOfSaleAddress:addressPointOfSale3 coordinatePointOfSale:CLLocationCoordinate2DMake(48.57543532, 35.06997) category:categoryPointOfSale1 starsRate:3.2 aboutCompanyString:@"О Мотыльке"];
        [self.testCoordinates addObject:pointOfSale3];
        [pointOfSale4 updatePointOfSaleForMapViewWithID:4 idCompany:3 namePointOfSale:@"Бар \"Братишка\"" pointOfSaleAddress:addressPointOfSale4 coordinatePointOfSale:CLLocationCoordinate2DMake(48.52543532, 35.01997) category:categoryPointOfSale3 starsRate:5.0 aboutCompanyString:@"О Братишке"];
        [self.testCoordinates addObject:pointOfSale4];
        
        //Moscow
        [pointOfSale5 updatePointOfSaleForMapViewWithID:5 idCompany:4 namePointOfSale:@"Кафе \"Живность\"" pointOfSaleAddress:addressPointOfSale5 coordinatePointOfSale:CLLocationCoordinate2DMake(55.7122222, 37.6455556) category:categoryPointOfSale1 starsRate:3.3 aboutCompanyString:@"О Живности"];
        [self.testCoordinates addObject:pointOfSale5];
        [pointOfSale6 updatePointOfSaleForMapViewWithID:6 idCompany:5 namePointOfSale:@"Пекарня \"Булка\"" pointOfSaleAddress:addressPointOfSale6 coordinatePointOfSale:CLLocationCoordinate2DMake(55.7422222, 37.6855556) category:categoryPointOfSale2 starsRate:4.4 aboutCompanyString:@"О Булке"];
        [self.testCoordinates addObject:pointOfSale6];
        [pointOfSale7 updatePointOfSaleForMapViewWithID:7 idCompany:5 namePointOfSale:@"Кафе \"Буйный жук\"" pointOfSaleAddress:addressPointOfSale7 coordinatePointOfSale:CLLocationCoordinate2DMake(55.7922222, 37.5555555) category:categoryPointOfSale1 starsRate:1.2 aboutCompanyString:@"О Буйном жуке"];
        [self.testCoordinates addObject:pointOfSale7];
        [pointOfSale8 updatePointOfSaleForMapViewWithID:8 idCompany:6 namePointOfSale:@"Бар \"Сердце пацана\"" pointOfSaleAddress:addressPointOfSale8 coordinatePointOfSale:CLLocationCoordinate2DMake(55.8222222, 37.5155555)  category:categoryPointOfSale3 starsRate:5.0 aboutCompanyString:@"О Сердце пацана"];
        [self.testCoordinates addObject:pointOfSale8];
        
        //Piter
        [pointOfSale9 updatePointOfSaleForMapViewWithID:9 idCompany:8 namePointOfSale:@"Кафе \"Сахарок\"" pointOfSaleAddress:addressPointOfSale5 coordinatePointOfSale:CLLocationCoordinate2DMake(59.8044444, 30.2041667) category:categoryPointOfSale1 starsRate:4.3 aboutCompanyString:@"О Сахарке"];
        [self.testCoordinates addObject:pointOfSale9];
        [pointOfSale10 updatePointOfSaleForMapViewWithID:10 idCompany:8 namePointOfSale:@"Пекарня \"Бублик\"" pointOfSaleAddress:addressPointOfSale6 coordinatePointOfSale:CLLocationCoordinate2DMake(59.8844444, 30.2941667) category:categoryPointOfSale2 starsRate:2.4 aboutCompanyString:@"О Бублике"];
        [self.testCoordinates addObject:pointOfSale10];
        [pointOfSale11 updatePointOfSaleForMapViewWithID:11 idCompany:9 namePointOfSale:@"Кафе \"Спокойный кролик\"" pointOfSaleAddress:addressPointOfSale7 coordinatePointOfSale:CLLocationCoordinate2DMake(59.8144444, 30.2541667) category:categoryPointOfSale1 starsRate:4.2 aboutCompanyString:@"О Спокойном кролике"];
        [self.testCoordinates addObject:pointOfSale11];
        [pointOfSale12 updatePointOfSaleForMapViewWithID:12 idCompany:10 namePointOfSale:@"Бар \"Ранимая душа\"" pointOfSaleAddress:addressPointOfSale8 coordinatePointOfSale:CLLocationCoordinate2DMake(59.9344444, 30.3341667)  category:categoryPointOfSale3 starsRate:5.0 aboutCompanyString:@"О ранимой душе"];
        [self.testCoordinates addObject:pointOfSale12];
    
    //Piter
    [pointOfSale9 updatePointOfSaleForMapViewWithID:9 idCompany:8 namePointOfSale:@"Кафе \"Сахарок\"" pointOfSaleAddress:addressPointOfSale5 coordinatePointOfSale:CLLocationCoordinate2DMake(59.8044444, 30.2041667) category:categoryPointOfSale1 starsRate:4.3 aboutCompanyString:@"О Сахарке"];
    [self.testCoordinates addObject:pointOfSale9];
    [pointOfSale10 updatePointOfSaleForMapViewWithID:10 idCompany:8 namePointOfSale:@"Пекарня \"Бублик\"" pointOfSaleAddress:addressPointOfSale6 coordinatePointOfSale:CLLocationCoordinate2DMake(59.8844444, 30.2941667) category:categoryPointOfSale2 starsRate:2.4 aboutCompanyString:@"О Бублике"];
    [self.testCoordinates addObject:pointOfSale10];
    [pointOfSale11 updatePointOfSaleForMapViewWithID:11 idCompany:9 namePointOfSale:@"Кафе \"Спокойный кролик\"" pointOfSaleAddress:addressPointOfSale7 coordinatePointOfSale:CLLocationCoordinate2DMake(59.8144444, 30.2541667) category:categoryPointOfSale1 starsRate:4.2 aboutCompanyString:@"О Спокойном кролике"];
    [self.testCoordinates addObject:pointOfSale11];
    [pointOfSale12 updatePointOfSaleForMapViewWithID:12 idCompany:10 namePointOfSale:@"Бар \"Ранимая душа\"" pointOfSaleAddress:addressPointOfSale8 coordinatePointOfSale:CLLocationCoordinate2DMake(59.9344444, 30.3341667)  category:categoryPointOfSale3 starsRate:5.0 aboutCompanyString:@"О ранимой душе"];
    [self.testCoordinates addObject:pointOfSale12];
    
//    [self plot:self.testCoordinates];
}

-(void)setButtonsGraphics
{
    menuButtonsYPosition.constant = 0.0;
    {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), NO, scaleScreen);
        [WikiBonusStyleKit drawAdminButtonIconWithFrame:CGRectMake(0, 0, 35, 35)];
        [self.buttonPersonalCabinet setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
        UIGraphicsEndImageContext();
        [self.buttonPersonalCabinet centerVerticallyWithPadding:1.0f];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), NO, scaleScreen);
        [WikiBonusStyleKit drawQrButtonIconWithFrame:CGRectMake(0, 0, 35, 35)];
        [self.buttonScanQR setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
        UIGraphicsEndImageContext();
        [self.buttonScanQR centerVerticallyWithPadding:1.0f];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(35, 35), NO, scaleScreen);
        [WikiBonusStyleKit drawSettingsIconWithFrame:CGRectMake(0, 0, 35, 35)];
        [self.buttonSettings setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
        UIGraphicsEndImageContext();
        [self.buttonSettings centerVerticallyWithPadding:1.0f];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(22, 20), NO, scaleScreen);
        [WikiBonusStyleKit drawFilterIconWithFrame:CGRectMake(0, 0, 22, 20)];
        [self.barButtonFilter setImage: UIGraphicsGetImageFromCurrentImageContext()];
        UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(22, 20), NO, scaleScreen);
        [WikiBonusStyleKit drawCataloglistIconWithFrame:CGRectMake(0, 0, 22, 20)];
        [self.barButtonList setImage: UIGraphicsGetImageFromCurrentImageContext()];
        UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContextWithOptions(self.buttonTrash.bounds.size, NO, scaleScreen);
        [WikiBonusStyleKit drawTrashIconWithFrame:self.buttonTrash.bounds];
        [self.buttonTrash setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
        UIGraphicsEndImageContext();
    }
}

-(void)setTestGifts
{
    _testGiftsArray = [NSMutableArray new];
    _testGiftsFiltred = [NSMutableArray new];
    Gift *testGift1= [Gift new];
    [testGift1 updateGiftWithIDGift:1 nameGift:@"Тортик в подарок" description:@"Соберите 3 фрагмента и получите в подарок бесплатный тортик от кондитерской Карамель" image:[UIImage imageNamed:@"gift.png"] likesCount:12 reviewsCount:3 idPointOfSale:1 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:NO procentFull:0.75];
    [_testGiftsArray addObject:testGift1];
    Gift *testGift2= [Gift new];
    [testGift2 updateGiftWithIDGift:2 nameGift:@"Бокал пива в подарок" description:@"Соберите все фрагменты и получите бесплатный бокал пива" image:[UIImage imageNamed:@"gift.png"] likesCount:4 reviewsCount:1 idPointOfSale:4 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:YES procentFull: 1.0];
    [_testGiftsArray addObject:testGift2];
    Gift *testGift3= [Gift new];
    [testGift3 updateGiftWithIDGift:3 nameGift:@"Кофе в подарок" description:@"Соберите все фрагменты и получите кофе в подарок" image:[UIImage imageNamed:@"gift.png"] likesCount:4 reviewsCount:1 idPointOfSale:3 dateDead:[NSDate date] arrayFragments:_testFragmentArray inWishes:NO isRecieved:NO procentFull: 0.0];
    [_testGiftsArray addObject:testGift3];
    
}

-(void)setTestFragments
{
    _testFragmentArray = [NSMutableArray new];
    Fragmen* testFragment1 = [Fragmen new];
    [testFragment1 updateFragmentWithID:1 nameFragment:@"First" imageFragment:[UIImage imageNamed:@"fragment1.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment1];
    Fragmen* testFragment2 = [Fragmen new];
    [testFragment2 updateFragmentWithID:1 nameFragment:@"Second" imageFragment:[UIImage imageNamed:@"fragment2.png"] dateDead:[NSDate date] isReceived:NO];
    [_testFragmentArray addObject:testFragment2];
    Fragmen* testFragment3 = [Fragmen new];
    [testFragment3 updateFragmentWithID:1 nameFragment:@"Third" imageFragment:[UIImage imageNamed:@"fragment3.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment3];
    Fragmen* testFragment4 = [Fragmen new];
    [testFragment4 updateFragmentWithID:1 nameFragment:@"Fourth" imageFragment:[UIImage imageNamed:@"fragment4.png"] dateDead:[NSDate date] isReceived:YES];
    [_testFragmentArray addObject:testFragment4];
}

#pragma mark Get points of sale

-(void)getAllPointsOfSale
{
    self.arrayAllPointsOfSale = [NSMutableArray new];
    
    NSDictionary* params = @{WB_SID : [[NSUserDefaults standardUserDefaults] objectForKey:WB_SID],
                             };
    
    
    [WBAPIClient pointsOfSaleListWithParams:params complete:^(id responseObject, NSError *error) {
        if (error)
        {
            NSLog(@"%@", [error.userInfo objectForKey:@"message"]);
            [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"errorLoadingData", nil) message:DPLocalizedString(@"repeatOperationLater", nil)
                              cancelButtonTitle:DPLocalizedString(@"close", nil)]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 1)
                 {
                 }
             }];
            
        }
        else
        {
            NSLog(@"%@", responseObject);
            self.arrayAllPointsOfSale = [WBParser getArrayPointsOfSale:responseObject];
          [self plot: self.arrayAllPointsOfSale];
        }
 
    }];
}

#pragma mark - Fiter view methods

- (IBAction)showFilterTableView:(id)sender
{
    if (_isShowedFilter == NO)
    {
        _isShowedFilter = YES;
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        _filterView.view.alpha = 0.0;

        menuButtonsYPosition.constant = -50.0;
        if(![self.view.subviews containsObject:_filterView.view])
        [self.view addSubview:_filterView.view];
        [self.view bringSubviewToFront:_filterView.view];
         [_filterView.view setHidden:NO];
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionLayoutSubviews
                         animations:^{_filterView.view.alpha = 1.0;
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
    }
}

-(void)hideFilterViewAndUpdatePointsWithChoosenCategoriesArray:(NSMutableArray*)currentCategoriesArray andFilterString:(NSString*)filterString
{
    _isShowedFilter = NO;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    menuButtonsYPosition.constant = 0.0;
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [_filterView.view setAlpha:0.0];
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [_filterView.view setHidden:YES];
        [self.view endEditing:YES];
        self.arrayAcceptedCategories = currentCategoriesArray;
        [self filtratePointsOfSaleWithMutableArrayPoints:self.arrayAllPointsOfSale mutableArrayAcceptedCategories:currentCategoriesArray filerString:filterString];
    }];
}

- (void)filtratePointsOfSaleWithMutableArrayPoints:(NSMutableArray*)arrayPointsOfSale mutableArrayAcceptedCategories:(NSMutableArray*)arrayAcceptedCategories filerString:(NSString*)filterString
{
    [self.mapViewPartners removeAnnotations:self.mapViewPartners.annotations];
    [self.clusteringManager removeAnnotations:self.clusteringManager.allAnnotations];
    [self.testCoordinatesFiltred removeAllObjects];
    NSMutableString *stringForFilterlabel = [NSMutableString stringWithCapacity:1];
    NSMutableString *stringSearchAndIDFilterPredicate = [NSMutableString stringWithCapacity:1];
    self.arrayAcceptedCategories = arrayAcceptedCategories;
    
    if (arrayAcceptedCategories.count>0 || filterString.length>0)
    {
        if (arrayAcceptedCategories.count>0)
        {
            [stringSearchAndIDFilterPredicate appendString:@"("];
            for (CategoryPointOfSale* tmpCategory  in self.arrayAcceptedCategories)
            {
                if ([tmpCategory isEqual:[self.arrayAcceptedCategories lastObject]])
                {
                    [stringSearchAndIDFilterPredicate appendString:[NSString stringWithFormat:@"SELF.categotyPointOfSale.idCategory = %ld ", (long)tmpCategory.idCategory]];
                    
                }
                else
                {
                    [stringSearchAndIDFilterPredicate appendString:[NSString stringWithFormat:@"SELF.categotyPointOfSale.idCategory = %ld || ", (long)tmpCategory.idCategory]];
                }
                [stringForFilterlabel appendString:[NSString stringWithFormat:@"%@; ", tmpCategory.nameCategory]];
                
            }
            [stringSearchAndIDFilterPredicate appendString:@")"];
            if (filterString.length>0)
            {
                [stringSearchAndIDFilterPredicate appendString:@" && "];
            }
        }
        if (filterString.length>0)
        {
            [stringSearchAndIDFilterPredicate appendString:[NSString stringWithFormat:@"(SELF.namePointOfSale CONTAINS[c] '%@' || SELF.descriptionCompany CONTAINS[c] '%@')", filterString, filterString]];
            [stringForFilterlabel appendString:[NSString stringWithFormat:@"%@%@;", DPLocalizedString(@"search", nil), filterString]];
        }
        
        NSPredicate* predicate =[NSPredicate predicateWithFormat:[stringSearchAndIDFilterPredicate copy]];
        self.testCoordinatesFiltred =[[arrayPointsOfSale filteredArrayUsingPredicate:predicate] mutableCopy];
        if (self.testCoordinatesFiltred.count == 0)
        {
            [self plot:self.arrayAllPointsOfSale];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 300*NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"searchNoResults", nil) message:nil
                                  cancelButtonTitle:DPLocalizedString(@"close", nil)]
                 showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                     if (buttonIndex == 1)
                     {
                     }
                 }];
            });
        }
            self.labelFiltresNames.text = stringForFilterlabel;
            [self.labelFiltresNames sizeToFit];
            self.filterInfoHeight.constant = 22.0 + self.labelFiltresNames.frame.size.height;
            [self plot:_testCoordinatesFiltred];
            [self filtrateGiftsWithMutableArrayPoints:self.testCoordinatesFiltred];
    }
    else
    {
        [self resetFilterData];
        [self plot:self.arrayAllPointsOfSale];
    }
}

-(void)filtrateGiftsWithMutableArrayPoints:(NSMutableArray*)arrayPointsOfSale
{
    [_testGiftsFiltred removeAllObjects];
    for (Gift *tmpGift in _testGiftsArray)
    {
        NSLog(@"gift.idPointOfSale %ld",(long)tmpGift.idPointOfSale);
        for (PointOfSale* tmpPointOfSale in arrayPointsOfSale)
        {
            NSLog(@"tmpPointOfSale.idPointOfSale %ld",(long)tmpPointOfSale.idPointOfSale);
            if (tmpGift.idPointOfSale == tmpPointOfSale.idPointOfSale)
            {
                [self.testGiftsFiltred addObject:tmpGift];
            }
        }
    }
    _giftViewCatalog.giftsArray = self.testGiftsFiltred;
    [_giftViewCatalog.tableViewMyGifts reloadData];
}

#pragma mark - work with MapView

- (CLLocationDistance)getRadius
{
    CLLocationCoordinate2D centerCoor = [self getCenterCoordinate];
    // init center location from center coordinate
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    
    CLLocationCoordinate2D topCenterCoor = [self getTopCenterCoordinate];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    
    return radius;
}

- (CLLocationCoordinate2D)getCenterCoordinate
{
    CLLocationCoordinate2D centerCoor = [self.mapViewPartners centerCoordinate];
    return centerCoor;
}

- (CLLocationCoordinate2D)getTopCenterCoordinate
{
    // to get coordinate from CGPoint of your map
    CLLocationCoordinate2D topCenterCoor = [self.mapViewPartners convertPoint:CGPointMake(self.mapViewPartners.frame.size.width / 2.0f, 0) toCoordinateFromView:self.mapViewPartners];
    return topCenterCoor;
}


- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation
{

}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self.locationManager stopUpdatingLocation];
    [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error", nil) message:DPLocalizedString(@"unableDetermineLocation", nil)
                          cancelButtonTitle:@"Закрыть"]
                          showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {}];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *const AnnotatioViewReuseID = @"AnnotatioViewReuseID";
    
    UIImageView *pinView = nil;
    self.calloutView = nil;
        if ([annotation isKindOfClass:[MKAnnotationCustom class]])
        {
            PartnersPinAnnotationView *annotationView = (PartnersPinAnnotationView *)[self.mapViewPartners dequeueReusableAnnotationViewWithIdentifier:AnnotatioViewReuseID];
        MKAnnotationCustom *pinAnnotation = (MKAnnotationCustom*) annotation;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(40, 40), NO, scaleScreen);
        [WikiBonusStyleKit drawPinDisableWithFrame:CGRectMake(0, 0, 40, 40) pressed:NO procentFull:pinAnnotation.procent];
        UIImage *pinImage = UIGraphicsGetImageFromCurrentImageContext();
        annotationView.image = pinImage;
        UIGraphicsEndImageContext();
        pinView = [[UIImageView alloc] initWithImage:pinImage];
        self.calloutView = [[[NSBundle mainBundle] loadNibNamed:@"PopUpView" owner:self options:nil] firstObject];
        self.calloutView.delegate = self;
        self.calloutView.backgroundColor = [UIColor clearColor];
        [self.calloutView updatePopUpViewDataWithPinAnnotation:pinAnnotation];
        self.calloutView.starRate = pinAnnotation.starRate;
        annotationView = [[PartnersPinAnnotationView alloc] initWithAnnotation:annotation
                                                               reuseIdentifier:AnnotatioViewReuseID
                                                                       pinView:pinView
                                                                   calloutView:self.calloutView
                                                                      settings:[PartnersPinAnnotationSettings defaultSettings]];
            return annotationView;
        }
        else if ([annotation isKindOfClass:[FBAnnotationCluster class]])
        {
            MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotatioViewReuseID];
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotatioViewReuseID];
            FBAnnotationCluster *cluster = (FBAnnotationCluster *)annotation;
            cluster.title = [NSString stringWithFormat:@"%lu", (unsigned long)cluster.annotations.count];
            
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(40, 40), NO, scaleScreen);
            [WikiBonusStyleKit drawPinViewClusterWithFrame:CGRectMake(0, 0, 40, 40) pressed:NO pinCount:[NSString stringWithFormat:@"%lu", (unsigned long)cluster.annotations.count]];
            UIImage *pinImage = UIGraphicsGetImageFromCurrentImageContext();
            annotationView.image = pinImage;
            UIGraphicsEndImageContext();
            
            return annotationView;
        }
    
    // This is how you can check if annotation is a cluster
    
    
    return [MKAnnotationView new];
}


- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if ([view isKindOfClass:[PartnersPinAnnotationView class]])
    {
        
        [((PartnersPinAnnotationView *)view)hideCalloutView];
        view.layer.zPosition = -1;
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view isKindOfClass:[PartnersPinAnnotationView class]])
    {
        [mapView setCenterCoordinate:view.annotation.coordinate animated:YES];
        [((PartnersPinAnnotationView *)view)showCalloutView];
        view.layer.zPosition = 0;
    }
    
    if ([view.annotation isKindOfClass:[FBAnnotationCluster class]])
    {
        [mapView setCenterCoordinate:view.annotation.coordinate animated:YES];
        MKCoordinateRegion mapRegion;
        mapRegion.center = view.annotation.coordinate;
        
        mapRegion.span.latitudeDelta = mapView.region.span.latitudeDelta/3;
        mapRegion.span.longitudeDelta= mapView.region.span.longitudeDelta/3;
        
        [mapView setRegion:mapRegion animated: YES];
    }
}

- (void)plot:(NSArray *)objectsToPlot
{
//    id userLocation = [self.mapViewPartners userLocation];
//    if ( userLocation != nil ) {
//        [self.mapViewPartners addAnnotation:userLocation]; // will cause user location pin to blink
//    }
    
    
    NSMutableArray *tmpArrayAnnotations = [NSMutableArray new];
    
    for (PointOfSale* tmpPartner  in objectsToPlot)
    {
        MKAnnotationCustom *annotationPartner = [[MKAnnotationCustom alloc] initWithPointOfSale:tmpPartner];
        [tmpArrayAnnotations addObject:annotationPartner];
    }
    if(!self.clusteringManager)
    {
        self.clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:tmpArrayAnnotations];
        self.clusteringManager.delegate = self;
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 40*NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
            [self.clusteringManager setAnnotations:tmpArrayAnnotations];
            [self mapView:self.mapViewPartners regionDidChangeAnimated:YES];
        });

        
    }
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    NSLog(@"%f", [self getRadius]);
        [[NSOperationQueue new] addOperationWithBlock:^{
            double scale = self.mapViewPartners.bounds.size.width / self.mapViewPartners.visibleMapRect.size.width;
            NSArray *annotations = [self.clusteringManager clusteredAnnotationsWithinMapRect:mapView.visibleMapRect withZoomScale:scale];
            
            [self.clusteringManager displayAnnotations:annotations onMapView:mapView];
        }];
   
}

-(void)showPointOfSaleViewControllerWithPointOfSale:(PointOfSale *)pointOfSale
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RestaurantViewController *pointOfSaleViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"RestaurantViewController"];
    pointOfSaleViewController.pointsOfSale = pointOfSale;
    [self.navigationController pushViewController:pointOfSaleViewController animated:YES];
}

- (CGFloat)cellSizeFactorForCoordinator:(FBClusteringManager *)coordinator
{
    return 1.5;
}

#pragma mark Pressed button methods

- (IBAction)buttonTrashPressed:(id)sender
{
    [self resetFilterData];
    for (NSInteger i=0; i<self.arrayAllCategories.count; i++)
    {
        [_filterView.tableViewFilterObjects deselectRowAtIndexPath:[NSIndexPath indexPathWithIndex:i] animated:NO];
    }
    [_filterView removeFromParentViewController];
    [self setFilterView];
}

-(void)resetFilterData
{
    self.labelFiltresNames.text = nil;
    self.arrayAcceptedCategories = nil;
    self.filterInfoHeight.constant = 0.0;
    _giftViewCatalog.giftsArray = _testGiftsArray;
    [_giftViewCatalog.tableViewMyGifts reloadData];
    
    [self.view layoutIfNeeded];
    [self plot:self.arrayAllPointsOfSale];
}

- (IBAction)buttonGiftsListPressed:(id)sender
{
    if (_isShowedGiftsList)
    {
        _isShowedGiftsList = NO;
        self.navigationController.topViewController.title = DPLocalizedString(@"partnersMap", nil);
        [self.view bringSubviewToFront:self.mapViewPartners];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(22, 20), NO, scaleScreen);
        [WikiBonusStyleKit drawCataloglistIconWithFrame:CGRectMake(0, 0, 22, 20)];
        [self.barButtonList setImage: UIGraphicsGetImageFromCurrentImageContext()];
        UIGraphicsEndImageContext();
    }
    else
    {
        _isShowedGiftsList = YES;
        
        [self.view bringSubviewToFront:self.containerView];
        self.navigationController.topViewController.title = DPLocalizedString(@"giftCatalog", nil);
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(26, 24), NO, scaleScreen);
        [WikiBonusStyleKit drawLocationBigIconWithFrame:CGRectMake(0, 0, 26, 24)];
        [self.barButtonList setImage: UIGraphicsGetImageFromCurrentImageContext()];
        UIGraphicsEndImageContext();
    }
}

- (IBAction)buttonQRCodePressed:(id)sender
{

    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
            [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error", nil) message:DPLocalizedString(@"noAccessCamera", nil)
                              cancelButtonTitle:DPLocalizedString(@"cancel", nil)]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 1)
                 {
                     // delete
                 }
             }];
    }
    else
    {
        [self.navigationController pushViewController:_scanner animated:YES];
//        [self presentViewController:_scanner animated:YES completion:nil];
    }

    
}

@end
