//
//  MyDesireTableViewCell.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/12/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDesiresTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelNameDesire;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhotoDesire;
@property (weak, nonatomic) IBOutlet UILabel *labelNamePartner;
@property (weak, nonatomic) IBOutlet UILabel *labelAdressPartner;
@property (weak, nonatomic) IBOutlet UILabel *labelDistanceToUser;


@end
