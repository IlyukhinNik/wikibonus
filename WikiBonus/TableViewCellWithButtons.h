//
//  TableViewCellWithButtons.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/23/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellWithButtons : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *buttonSendGift;
@property (weak, nonatomic) IBOutlet UIButton *buttonTakeGift;

@end
