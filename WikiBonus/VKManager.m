//
//  VKManager.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 3/1/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "VKManager.h"

#define VK_APP_KEY @"5124735"

static NSArray  * SCOPE = nil;

@implementation VKManager

-(id)init
{
    self = [super init];
    
    if (self)
    {
        SCOPE = @[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_EMAIL];
        VKSdk *sdkInstance = [VKSdk initializeWithAppId:VK_APP_KEY];
        [sdkInstance registerDelegate:self];
        [sdkInstance setUiDelegate:self];
    }
    return self;
}

-(BOOL)logIn
{
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized)
        {
            _vkAccessToken = [VKSdk accessToken];
            
        }
        else if (state == VKAuthorizationInitialized)
        {
            [VKSdk authorize:SCOPE];
        }
    }];
    return [self isLoggedIn];
}

-(BOOL)isLoggedIn
{
    if (self.vkAccessToken)
        return YES;
    else
        return NO;
}

-(void)logOut
{
    [VKSdk forceLogout];
}

#pragma mark - VK delegate methods

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    if (!result.error)
    {
        self.vkAccessToken = result.token;
        NSLog(@"VK login successfully");
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации VK" message:@"Повторите действие позже."
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         }];
    }
    
}

- (void)vkSdkUserAuthorizationFailed
{
    [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации VK" message:@"Повторите действие позже."
                      cancelButtonTitle:@"Закрыть"]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
     }];
}


- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    //    [self pressedButtonVKRegistration:nil];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    self.vkAccessToken = newToken;
    NSLog(@"%@", self.vkAccessToken.userId);
    //    [self getVKUserData];
}
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    //    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    [[[UIAlertView alloc] initWithTitle:nil message:@"Доступ запрещен" delegate:self cancelButtonTitle:@"Закрыть" otherButtonTitles:nil] show];
}

#pragma mark - VK users data methods

//return VKUsersArray

-(VKUsersArray*)getFriendsArray
{
    __block VKUsersArray *users;
    VKRequest *dataRequest = [[VKApi friends] get];
    dataRequest.waitUntilDone = YES;
    [dataRequest executeWithResultBlock:^(VKResponse *response) {
        users = response.parsedModel;
    }                    errorBlock:^(NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка  загрузки данных." message:@"Повторите действие позже."
                          cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
             if (buttonIndex == 1)
             {
                 // delete
             }
         }];
    }];
    return users;
}

-(VKUser*)getVKUser

{
    __block VKUser * _localUser = [VKUser new];
    VKRequest* dataRequest = [[VKApi users] get];
    [dataRequest executeWithResultBlock:^(VKResponse *response)
     {
         _localUser = [[response.json objectAtIndex:0] parsedModel];
     } errorBlock:^(NSError *error) {
         [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации VK" message:@"Повторите действие позже."
                           cancelButtonTitle:@"Закрыть"]
          showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
              if (buttonIndex == 1)
              {
                  // delete
              }
          }];
     }];
    return _localUser;
}

-(NSDictionary*)getUserData

{
      __block NSDictionary* _userData = [NSDictionary new];
    VKRequest* dataRequest = [[VKApi users] get];
    [dataRequest executeWithResultBlock:^(VKResponse *response)
     {
         _userData = [[response.json objectAtIndex:0] json];
     } errorBlock:^(NSError *error) {
         [[[UIAlertView alloc] initWithTitle:@"Ошибка авторизации VK" message:@"Повторите действие позже."
                           cancelButtonTitle:@"Закрыть"]
          showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
              if (buttonIndex == 1)
              {
                  // delete
              }
          }];
     }];
    return _userData;
}


-(void)shareVKText:(NSString*)text andImagesArray:(NSArray*)imagesArray andUrl:(NSURL*)url andVC:(UIViewController*)vc
{
    VKShareDialogController *shareDialog = [VKShareDialogController new];
    shareDialog.text = text;
    shareDialog.uploadImages = imagesArray;
    shareDialog.shareLink = [[VKShareLink alloc] initWithTitle:@"" link:[NSURL URLWithString:@"https://vk.com/dev/ios_sdk"]];
    [shareDialog setCompletionHandler:^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
        [vc dismissViewControllerAnimated:YES completion:nil];
    }];
    [vc presentViewController:shareDialog animated:YES completion:nil];
}


@end
