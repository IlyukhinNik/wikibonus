//
//  SettingsViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/11/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppDelegate.h"
#import "WBAPIClient.h"
#import "MBProgressHUD.h"
#import "SettingsTableViewCell.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface SettingsViewController () <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
    NSArray* _settingsArray;
    NSArray* _settingsIconArray;
    NSUserDefaults *userDefaults;
    NSMutableArray* _imagesArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableViewSettings;

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = DPLocalizedString(@"settings", nil);
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    _settingsArray = @[DPLocalizedString(@"adjustSearchRadius", nil), DPLocalizedString(@"writeToDevelopers", nil), DPLocalizedString(@"termsOfUse", nil), DPLocalizedString(@"exit", nil)];
    _imagesArray = [[NSMutableArray alloc] initWithCapacity:4];
    [self setImageArrayForSettingsTableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setImageArrayForSettingsTableView
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawRadiusIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [_imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawLetterIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [_imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawRulesIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [_imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(30, 30), NO, scaleScreen);
    [WikiBonusStyleKit drawLogoutButtonIconWithFrame:CGRectMake(0, 0, 30, 30)];
    [_imagesArray addObject:UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _settingsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsTableViewCell* cell = [self.tableViewSettings dequeueReusableCellWithIdentifier:@"cellSettings"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        cell = [[SettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellSettings"];
    }
    cell.labelSettings.text = [_settingsArray objectAtIndex:indexPath.row];
    cell.imageSettings.image = [_imagesArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"inDevelopment", nil) message:nil
                              cancelButtonTitle:DPLocalizedString(@"close", nil)]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 1)
                 {
                     // delete
                 }
             }];
        }
            break;
        case 1:
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                mail.mailComposeDelegate = self;
                [mail setSubject:@"Разработчику WikiBonus"];
                [mail setMessageBody:@"" isHTML:NO];
                [mail setToRecipients:@[@"support@cuborubo.com"]];
                
                [self presentViewController:mail animated:YES completion:NULL];
            }
            else
            {
                NSLog(@"This device cannot send email");
            }
        }
            break;
        case 2:
        {
            [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"inDevelopment", nil) message:nil
                              cancelButtonTitle:DPLocalizedString(@"close", nil)]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 1)
                 {
                     // delete
                 }
             }];
        }
            break;
        case 3:
        {
            [self logout];
        }
            break;

    }
}

#pragma mark - Logout and delete data about User

-(void)logout
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [VKSdk forceLogout];
        [[FBSDKLoginManager new] logOut];
        [WBAPIClient exitFromAccountWithComplete:^(id responseObject, NSError *error) {
            if (error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                NSLog(@"%@", [error.userInfo objectForKey:@"message"]);
                [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"enterError", nil) message:DPLocalizedString(@"repeatOperationLater", nil)
                                  cancelButtonTitle:DPLocalizedString(@"close", nil)]
                 showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                     if (buttonIndex == 1)
                     {
                         // delete
                     }
                 }];
                
            }
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths firstObject];
                NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
                NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"UserInfo" ofType:@"plist"];
                NSDictionary *originalPlist = [NSDictionary dictionaryWithContentsOfFile:bundlePath];
                [originalPlist writeToFile:path atomically:YES];
                [userDefaults removeObjectForKey:@"SID"];
                [userDefaults synchronize];
                AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
                
                UINavigationController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"AutorizationRegistrationViewController"];
            
                [userDefaults synchronize];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                [UIView transitionWithView:appDelegateTemp.window duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                    appDelegateTemp.window.rootViewController  = rootController;
                } completion:nil];
            }
            
        }];

    });
    
    
}
@end
