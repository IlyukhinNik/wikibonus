//
//  FragmentViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/25/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "FragmentViewController.h"
#import "UIButton+VerticalLayout.h"
#import "ImageViewBackgroundBorder.h"

@interface FragmentViewController ()
{
    IBOutlet UIButton *_buttonMoreGiftsFromPartner;
    IBOutlet UIButton *_buttonWatchAtMap;
    IBOutlet ImageViewBackgroundBorder *_imageViewDescriptionBorder;
    IBOutlet UIImageView *imageViewFragment;
    IBOutlet UILabel *labelNameFragment;
    IBOutlet UILabel *labelDescriptionFragment;
    IBOutlet UILabel *labelDateDead;
}
- (IBAction)buttonMoreGiftsPressed:(id)sender;
- (IBAction)buttonWatchAtMapPressed:(id)sender;
- (IBAction)buttonSendToFriendPressed:(id)sender;

@end

@implementation FragmentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setDataOfFragment];
}

-(void)setDataOfFragment
{
    _buttonMoreGiftsFromPartner.layer.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    _buttonMoreGiftsFromPartner.layer.borderWidth = 0.5f;
    _buttonWatchAtMap.layer.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    _buttonWatchAtMap.layer.borderWidth = 0.5f;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
    [WikiBonusStyleKit drawGiftIconWithFrame:CGRectMake(0, 0, 50, 50) pressed:NO];
    [_buttonMoreGiftsFromPartner setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
    [WikiBonusStyleKit drawLocationBigIconWithFrame:CGRectMake(0, 0, 50, 50)];
    [_buttonWatchAtMap setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
  
    [_buttonMoreGiftsFromPartner centerVerticallyWithPadding:15.0f];
    [_buttonWatchAtMap centerVerticallyWithPadding:15.0f];
    imageViewFragment.image = self.currentFragment.imageFragment;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonMoreGiftsPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Функционал в разработке" message:nil
                      cancelButtonTitle:@"Закрыть"]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if (buttonIndex == 1)
         {
             // delete
         }
     }];
}

- (IBAction)buttonWatchAtMapPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Функционал в разработке" message:nil
                      cancelButtonTitle:@"Закрыть"]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if (buttonIndex == 1)
         {
             // delete
         }
     }];
}

- (IBAction)buttonSendToFriendPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Функционал в разработке" message:nil
                      cancelButtonTitle:@"Закрыть"]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if (buttonIndex == 1)
         {
             // delete
         }
     }];
}
@end
