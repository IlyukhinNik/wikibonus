//
//  WBParser.h
//  WikiBonus
//
//  Created by куборубо on 04.11.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WBError.h"

#import "User.h"
#import "PoinOfSale.h"
#import "NSDictionary+Validate.h"
#import "CategoryPointOfSale.h"

@interface WBParser : NSObject

+(WBError*) errorFromResponseObject: (NSDictionary*) response;
+(User*) getUserFromDictionaryVK: (NSDictionary*) response;
+(User*) getUserFromDictionaryFB: (NSDictionary*) response;
+(User*) getUser: (NSDictionary*) response andEmail:(NSString*)email;
+(NSDictionary*) makeDictionaryForPList: (NSDictionary*) response andEmail:(NSString*)email;
+(User*) getUserFromPlistDictionary: (NSDictionary*) userInfoDictionary;

+(PointOfSale*) getPointOfSale: (NSDictionary*) response;
+(CategoryPointOfSale*) getCategoryPointOfSale: (NSDictionary*) response;
+(NSMutableArray*) getArrayPointsOfSale: (NSDictionary*) response;
@end
