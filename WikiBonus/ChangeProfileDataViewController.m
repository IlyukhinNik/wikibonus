//
//  ChangeProfileDataViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/9/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "ChangeProfileDataViewController.h"
#import "SexButton.h"
#import "WBParser.h"
#import <AVFoundation/AVFoundation.h>
#import "WBAPIClient.h"

@interface ChangeProfileDataViewController () <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSString *_sex;
    User* _localUser;
    NSArray *_pickerCityArray;
    UIPickerView *_pickerViewCity;
    NSString *_city;
    NSUserDefaults *_userDefaults;
}
@property (weak, nonatomic) IBOutlet UITextField * textFieldPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField * textFieldCity;
@property (strong, nonatomic) NSString *surname;
@property (nonatomic) IBOutlet UIDatePicker* datePicker;

@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhotoUser;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet SexButton *buttonMan;
@property (weak, nonatomic) IBOutlet SexButton *buttonWomen;
@property (weak, nonatomic) IBOutlet UITextField *textFieldBirthday;

- (IBAction)manButtonPressed:(id)sender;
- (IBAction)womenButtonPressed:(id)sender;
@end

@implementation ChangeProfileDataViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = DPLocalizedString(@"editing", nil);
    _pickerCityArray = [[NSArray alloc] initWithObjects: @"Днепропетровск", @"Киев", @"Полтава", @"Львов", @"Харьков", @"Житомир", nil];
    self.imageViewPhotoUser.layer.cornerRadius = self.imageViewPhotoUser.frame.size.height /2;;
    self.imageViewPhotoUser.layer.masksToBounds = YES;
    _localUser = [WBParser getUserFromPlistDictionary:[self loadUserDataFromPList]];
    self.imageViewPhotoUser.userInteractionEnabled = YES;
    UITapGestureRecognizer *profileGestureRecognizer= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePhoto)];
    profileGestureRecognizer.delegate = self;
    [self.imageViewPhotoUser addGestureRecognizer:profileGestureRecognizer];
    _sex = _localUser.gender;
    
    if (_localUser.photoLocalURL.length > 0)
    {
        [self.imageViewPhotoUser setImage:[self loadImagewithName:@"UserInfo.png"]];
    }
    else
    {
        UIGraphicsBeginImageContextWithOptions(self.imageViewPhotoUser.bounds.size, NO, scaleScreen);
        [WikiBonusStyleKit drawNoImageIconWithFrame:self.imageViewPhotoUser.bounds];
        [self.imageViewPhotoUser setImage:UIGraphicsGetImageFromCurrentImageContext()];
        UIGraphicsEndImageContext();
    }
    if ([_sex isEqualToString:@"male"])
    {
        [self.buttonMan isActive];
    }
    else if([_sex isEqualToString:@"female"])
    {
        [self.buttonWomen isActive];
    }
    self.textFieldFirstName.text = _localUser.firstNameUser;
    self.textFieldLastName.text = _localUser.lastNameUser;
    self.textFieldBirthday.text = _localUser.bdayUser;
    self.dateTextField.text = _localUser.bdayUser;
    self.textFieldCity.text = _localUser.cityUser;
    self.textFieldPhoneNumber.text = _localUser.phoneUser;

    UIToolbar *toolBarDate=[UIToolbar new];
    [toolBarDate setTintColor:[UIColor grayColor]];
    UIBarButtonItem *buttonDoneDate =  [[UIBarButtonItem alloc] initWithTitle:@"Готово" style: UIBarButtonItemStyleDone target:self action:@selector(updateTextFieldDate)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBarDate setItems:[NSArray arrayWithObjects:space, buttonDoneDate, nil]];
    [self.dateTextField setInputAccessoryView:toolBarDate];
    self.dateTextField.inputAccessoryView.frame = CGRectMake(0, 0, 320, 30);
    
    _pickerViewCity = [UIPickerView new];
    _pickerViewCity.dataSource = self;
    _pickerViewCity.delegate = self;
    [self.textFieldCity setInputView:_pickerViewCity];
    
    UIToolbar *toolBarCity = [UIToolbar new];
    [toolBarCity setTintColor:[UIColor grayColor]];
    UIBarButtonItem *buttonDoneCity =  [[UIBarButtonItem alloc] initWithTitle:DPLocalizedString(@"done", nil) style: UIBarButtonItemStyleDone target:self action:@selector(updateTextFieldCity)];
    [toolBarCity setItems:[NSArray arrayWithObjects:space, buttonDoneCity, nil]];
    [self.textFieldCity setInputAccessoryView:toolBarCity];
    self.textFieldCity.inputAccessoryView.frame = CGRectMake(0, 0, 320, 40);
    
    UIToolbar *toolBarPhone=[UIToolbar new];
    [toolBarPhone setTintColor:[UIColor grayColor]];
    UIBarButtonItem *buttonDonePhone =  [[UIBarButtonItem alloc] initWithTitle:DPLocalizedString(@"done", nil) style: UIBarButtonItemStyleDone target:self action:@selector(updateTextFieldPhoneNumber)];
    [toolBarPhone setItems:[NSArray arrayWithObjects:space, buttonDonePhone, nil]];
    [self.textFieldPhoneNumber setInputAccessoryView:toolBarPhone];
    self.textFieldPhoneNumber.inputAccessoryView.frame = CGRectMake(0, 0, 320, 40);
     _userDefaults =  [NSUserDefaults standardUserDefaults];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.datePicker = [UIDatePicker new];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.datePicker setDate:[NSDate date]];
    [self.datePicker setMaximumDate:[NSDate date]];
    [self.dateTextField setInputView:self.datePicker];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)pickerValueChanged:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIDatePicker *datePicker = (UIDatePicker *)sender;
        
        if ([datePicker.date compare:[NSDate date]] == NSOrderedDescending)
        {
            datePicker.date = [NSDate date];
        }
    });
}

- (NSDictionary*)loadUserDataFromPList
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths firstObject];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path])
    {
        NSLog(@"path doesn't exist. plist file will be copied to the path.");
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"UserInfo" ofType:@"plist"];
        if (bundlePath)
        {
            NSLog(@"file exists in the main bundle.");
            NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:bundlePath];
            NSLog(@"Bundle UserInfo.plist, %@", [resultDictionary description]);
            [fileManager copyItemAtPath:bundlePath toPath:path error:nil];
            NSLog(@"plist file is copied from main bundle to document directory");
        }
        else
        {
            NSLog(@"UserInfo.plist not found in main bundle.");
        }
    }
    NSDictionary *resultDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    NSLog(@"Loaded UserInfo.plist, %@", [resultDictionary description]);
    return resultDictionary;
}

#pragma mark - Save and load local photo methods
- (void)saveImage: (UIImage*)image withName:(NSString*)name
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithString:name]];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}

- (UIImage*)loadImagewithName:(NSString*)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithString:name] ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

#pragma Take photo methods
-(void)takePhoto
{
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Выберите действие"
                                                        delegate: self
                                                        cancelButtonTitle:DPLocalizedString(@"cancel", nil)
                                                        destructiveButtonTitle:@"Удалить фото"
                                                        otherButtonTitles:@"Сделать фото",@"Выбрать из галереи", nil];
    [actionSheet showInView:self.view];
}

- (void)showAlert
{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error", nil)
                                                    message:DPLocalizedString(@"noAccessCamera", nil)
                                                    delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
    [myAlertView show];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if(buttonIndex == 1)
        {
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                [self showAlert];
            }
            else
            {
                AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                if(authStatus == AVAuthorizationStatusAuthorized)
                {
                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.allowsEditing = YES; // YES
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [self presentViewController:picker animated:YES completion:NULL];
                }
                else if(authStatus == AVAuthorizationStatusDenied)
                {
                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error", nil)
                                                                          message:@"noAccessCamera"
                                                                         delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles: nil];
                    [myAlertView show];
                }
                else if(authStatus == AVAuthorizationStatusRestricted)
                {
                    // restricted, normally won't happen
                }
                else if(authStatus == AVAuthorizationStatusNotDetermined)
                {
                    // not determined?!
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        if(granted)
                        {
                            NSLog(@"Granted access to %@", AVMediaTypeVideo);
                        }
                        else
                        {
                            NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                        }
                    }];
                }
                else
                {
                    // impossible, unknown authorization status
                }

            }
        }
        else if(buttonIndex == 2)
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.delegate = self;
            [self presentViewController:picker animated:NO completion:nil];
        
        }
        else if (buttonIndex == 0)
        {
            UIGraphicsBeginImageContextWithOptions(self.imageViewPhotoUser.bounds.size, NO, scaleScreen);
            [WikiBonusStyleKit drawNoImageIconWithFrame:self.imageViewPhotoUser.bounds];
            [self.imageViewPhotoUser setImage:UIGraphicsGetImageFromCurrentImageContext()];
            UIGraphicsEndImageContext();
        }
    }];
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    self.imageViewPhotoUser.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)save:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [self saveInfo];
}

-(NSString*)surname
{
    return [self.textFieldFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(void)saveInfo
{
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
//    @{WB_AUTH_FIRST_NAME : self.textFieldFirstName.text.length > 0 ? self.textFieldFirstName.text : @"",
//                                 WB_AUTH_LAST_NAME : self.textFieldLastName.text.length > 0 ? self.textFieldLastName.text : @"",
//                                 WB_AUTH_GENDER : _sex,
//                                 WB_AUTH_CITY : self.textFieldCity.text.length > 0 ? self.textFieldCity.text : @"",
//                                 WB_AUTH_BDATE : self.dateTextField.text.length > 0 ? self.dateTextField.text : @"",
//                                 @"phone" : self.textFieldPhoneNumber.text.length > 0 ? self.textFieldPhoneNumber.text : @""};
    
    if(![_sex isEqualToString:_localUser.gender])
       [parameters setObject:_sex forKey:WB_AUTH_GENDER];
    
    if(![self.textFieldFirstName.text isEqualToString:_localUser.firstNameUser])
        [parameters setObject:self.textFieldFirstName.text forKey:WB_AUTH_FIRST_NAME];
    
    if(![self.textFieldLastName.text isEqualToString:_localUser.lastNameUser])
        [parameters setObject:self.textFieldLastName.text forKey:WB_AUTH_LAST_NAME];
    
    if(![self.textFieldCity.text isEqualToString:_localUser.cityUser])
        [parameters setObject:self.textFieldCity.text forKey:WB_AUTH_CITY];
    
    if(![self.dateTextField.text isEqualToString:_localUser.bdayUser])
        [parameters setObject:self.textFieldBirthday.text forKey:WB_AUTH_BDATE];

    
    [WBAPIClient changeProfileWithParameters:parameters complete:^(id responseObject, NSError *error) {
        if (error)
        {
            NSLog(@"%@", [error.userInfo objectForKey:@"message"]);
            [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"changingDataError", nil) message:DPLocalizedString(@"repeatOperationLater", nil)
                              cancelButtonTitle:DPLocalizedString(@"close", nil)]
             showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                 if (buttonIndex == 1)
                 {
                     // delete
                 }
             }];
        }
        else
        {
            [self saveImage:self.imageViewPhotoUser.image withName:@"UserInfo.png"];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserInfo.plist"];
#warning refactor  validate
            NSMutableDictionary *resultDictionary = [NSMutableDictionary dictionaryWithDictionary:[NSDictionary dictionaryWithContentsOfFile:path]];
            [resultDictionary setObject:@"UserInfo.png" forKey:@"avatar_local"];
            [resultDictionary setObject:self.textFieldFirstName.text forKey:@"first_name"];
            [resultDictionary setObject:self.textFieldLastName.text forKey:@"last_name"];
            [resultDictionary setObject:_sex forKey:@"sex"];
            [resultDictionary setObject:self.dateTextField.text forKey:@"birthday"];
            [resultDictionary setObject:self.textFieldPhoneNumber.text forKey:@"phone_user"];
            [resultDictionary setObject:self.textFieldCity.text forKey:@"city_name"];
            if (self.textFieldBirthday)
            {
                [resultDictionary setObject:self.textFieldBirthday forKey:@"birthday"];
            }
            [resultDictionary writeToFile:path atomically:YES];
            NSLog(@"Saved UserInfo.plist, %@", [resultDictionary description]);   
        }
        }];
    }

-(void)updateTextFieldDate
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    [self.dateTextField endEditing:YES];
    self.dateTextField.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:self.datePicker.date]];
    [self.dateTextField resignFirstResponder];
}

-(void)updateTextFieldCity
{
    [self.textFieldCity endEditing:YES];
    self.textFieldCity.text = _city;
    [self.dateTextField resignFirstResponder];
}

-(void)updateTextFieldPhoneNumber
{
    [self.textFieldPhoneNumber endEditing:YES];
    [self.textFieldPhoneNumber resignFirstResponder];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [_pickerCityArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return _pickerCityArray.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    // Handle the selection
    NSLog(@"%@",[_pickerCityArray objectAtIndex:row]);
    _city = [NSString stringWithFormat:@"%@",[_pickerCityArray objectAtIndex:row]];
    [self.textFieldCity setText:[_pickerCityArray objectAtIndex:row]];
}

- (IBAction)manButtonPressed:(id)sender
{
    [self.buttonWomen isNoActive];
    [self.buttonMan isActive];
    _sex = @"male";
}

- (IBAction)womenButtonPressed:(id)sender
{
    [self.buttonWomen isActive];
    [self.buttonMan isNoActive];
    _sex = @"female";
}

@end
