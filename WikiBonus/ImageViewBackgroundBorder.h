//
//  ImgavieBackgroundBorder.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/28/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewBackgroundBorder : UIImageView

-(void)setBorderWithWidth:(float)borderWidth;
@end
