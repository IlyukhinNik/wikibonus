//
//  TakeGiftSplashViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 1/28/16.
//  Copyright © 2016 CuboRubo. All rights reserved.
//

#import "TakeGiftSplashViewController.h"

@interface TakeGiftSplashViewController ()<UIGestureRecognizerDelegate>
{
    IBOutlet UILabel *labelTakeThing;
    IBOutlet UIImageView *imageViewThing;
    IBOutlet UILabel *labelNameThing;
    IBOutlet UIImageView *imageViewRate;
}
- (IBAction)buttonShowThingPressed:(id)sender;

@end

@implementation TakeGiftSplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(drawStarRate:)];
    [imageViewRate addGestureRecognizer:panGestureRecognizer];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(drawStarRate:)];
    [imageViewRate addGestureRecognizer:tapGestureRecognizer];

    
    UIGraphicsBeginImageContextWithOptions(imageViewRate.bounds.size, NO, scaleScreen);
    [WikiBonusStyleKit drawPointOfSaleStarRateWithFrame:imageViewRate.bounds starsCount:0.0];
    [imageViewRate setImage: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)drawStarRate:(UIGestureRecognizer *)panRateGestureRecognizer
{
   CGPoint touchLocation = [panRateGestureRecognizer locationInView:imageViewRate];
    NSInteger starRate;
    if (touchLocation.x/imageViewRate.bounds.size.width < 0.2)
    {
        starRate = 1.0;
    }
    else if (touchLocation.x/imageViewRate.bounds.size.width < 0.4)
    {
        starRate = 2.0;
    }
    else if (touchLocation.x/imageViewRate.bounds.size.width < 0.6)
    {
        starRate = 3.0;
    }
    else if (touchLocation.x/imageViewRate.bounds.size.width < 0.8)
    {
        starRate = 4.0;
    }
    else if (touchLocation.x/imageViewRate.bounds.size.width < 1.0)
    {
        starRate = 5.0;
    }
    UIGraphicsBeginImageContextWithOptions(imageViewRate.bounds.size, NO, scaleScreen);
    [WikiBonusStyleKit drawPointOfSaleStarRateWithFrame:imageViewRate.bounds starsCount:starRate];
    [imageViewRate setImage: UIGraphicsGetImageFromCurrentImageContext()];
    UIGraphicsEndImageContext();
    
    NSLog(@"%f", touchLocation.x);
}


- (IBAction)buttonShowThingPressed:(id)sender
{
}

@end
