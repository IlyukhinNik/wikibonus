//
//  FilterView.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/24/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FilterViewDelegate <NSObject>
@required

- (void)hideFilterViewAndUpdatePointsWithChoosenCategoriesArray:(NSMutableArray*)currentCategoriesArray andFilterString:(NSString*)filterString;

@end
@interface FilterView : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property (strong, nonatomic) NSMutableArray *arrayCurrentCategories;
@property (strong, nonatomic) id<FilterViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableViewFilterObjects;
@property (strong, nonatomic) NSArray *arrayKindOfPartners;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBarFilter;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonClose;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarPointsOfSale;

@end
