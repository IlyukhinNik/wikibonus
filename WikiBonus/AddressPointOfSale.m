//
//  AdressPointOfSale.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/4/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "AddressPointOfSale.h"

@implementation AddressPointOfSale

-(NSString*)addressToString
{
    NSString *addressString = [[NSString alloc] initWithFormat:@"%@, %@", self.cityName, self.address ];
    return addressString;
}

-(void)updateAddressPointOfSaleWhithCountryID:(NSInteger)countryID cityID:(NSInteger)cityID cityName:(NSString*)cityName address:(NSString*)address
{
    self.countryID = countryID;
    self.cityID = cityID;
    self.cityName = cityName;
    self.address = address;
}
@end
