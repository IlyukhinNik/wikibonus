//
//  GiftViewController.m
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/15/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import "GiftViewController.h"
#import "CollectionViewCellFragment.h"
#import "TPKeyboardAvoidingTableView.h"
#import "Fragmen.h"
#import "TableViewCellWithButtons.h"
#import "FragmentViewController.h"
#import "UIButton+VerticalLayout.h"
#import "ImageViewBackgroundBorder.h"


@interface GiftViewController () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
{
    NSArray *_arrayServices;
    IBOutlet UILabel *labelProcentGift;
    IBOutlet UILabel *labelGiftName;
    IBOutlet UILabel *labelDescription;
    Fragmen* _currentFragment;
    IBOutlet ImageViewBackgroundBorder *imageViewBackgroundBorder;
    IBOutlet UILabel *labelDateDead;
}

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableViewServices;
@property (weak, nonatomic) IBOutlet UIButton *buttonViewLikes;
@property (weak, nonatomic) IBOutlet UIButton *buttonViewReviews;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewFragments;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewProcentGift;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewServicesHeight;

@end

@implementation GiftViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self setImages];
    [self setGiftData];
    _arrayServices = @[@"Еще подарки от партнера",@"Посмотреть на карте"];
    if (self.gift.isReceived)
    {
        self.tableViewServicesHeight.constant = 208.0;
        [self.tableViewServices setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    else
    {
      self.tableViewServicesHeight.constant = 88.0;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

-(void)setGiftData
{
    self.navigationItem.title = self.gift.nameGift;
    labelGiftName.text = self.gift.nameGift;
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd.MM.YYYY"];
    labelDateDead.text = [NSString stringWithFormat:@"Действительно до %@",[dateFormatter stringFromDate:self.gift.dateDead]];
    labelDescription.text = self.gift.descriptionGift;
    [labelDescription sizeToFit];
}

-(void)setImages
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(40, 40), NO, scaleScreen);
    [WikiBonusStyleKit drawHeartButtonWithFrame:CGRectMake(0, 0, 40, 40) pressed:self.gift.inWishes];
    [self.buttonViewLikes setBackgroundImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal] ;
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(40, 40), NO, scaleScreen);
    [WikiBonusStyleKit drawReviewIconWithFrame:CGRectMake(0, 0, 40, 40) haveReviews:NO];
    [self.buttonViewReviews setBackgroundImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal] ;
    UIGraphicsEndImageContext();
    
    if (self.gift.procentFull > 0)
    {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(180, 180), NO, scaleScreen);
        [WikiBonusStyleKit drawProcentGiftWithProcentFull:self.gift.procentFull dem: 174  strokeWidth:6.0];
        self.imageViewProcentGift.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        labelProcentGift.text = [NSString stringWithFormat:@"%d%%", (int)ceilf(self.gift.procentFull*100)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)buttonHighlight:(id)sender
{
    UIButton *buttonClicked = (UIButton *)sender;
    
    UIView *colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    colorView.backgroundColor = [UIColor colorWithRed:228 green:112 blue:28 alpha:1];
    
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [buttonClicked setBackgroundImage:colorImage  forState:UIControlStateHighlighted];
    buttonClicked.tintColor = [UIColor whiteColor];
}

-(void)buttonNormal:(id)sender
{
    UIButton *buttonClicked = (UIButton *)sender;
    [buttonClicked setBackgroundColor:[UIColor whiteColor]];
    buttonClicked.tintColor = [UIColor colorWithRed:228 green:112 blue:28 alpha:1];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.gift.isReceived)
        return 2;
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (self.gift.isReceived)
    {
       if (section == 0)
           return [_arrayServices count];
       else
           return 1;
    }
    else
        return [_arrayServices count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 44.0;
    }
    else
    {
        return 120.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        UITableViewCell* cell = [self.tableViewServices dequeueReusableCellWithIdentifier:@"ServicesCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ServicesCell"];
        }
        cell.textLabel.text = [_arrayServices objectAtIndex:indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    if (indexPath.section == 1)
    {
        TableViewCellWithButtons* cellWithButtons = [self.tableViewServices dequeueReusableCellWithIdentifier:@"ButtonsCell"];
        if (cellWithButtons == nil)
        {
            cellWithButtons = [[TableViewCellWithButtons alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ButtonsCell"];
        }
        cellWithButtons.buttonTakeGift.layer.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
        cellWithButtons.buttonTakeGift.layer.borderWidth = 0.5f;
        cellWithButtons.buttonSendGift.layer.borderColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
        cellWithButtons.buttonSendGift.layer.borderWidth = 0.5f;
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
        [WikiBonusStyleKit drawGiftIconWithFrame:CGRectMake(0, 0, 50, 50) pressed:YES];
        [cellWithButtons.buttonTakeGift setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
        UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, scaleScreen);
        [WikiBonusStyleKit drawSendGiftWithFrame:CGRectMake(0, 0, 50, 50) pressed:NO];
        [cellWithButtons.buttonSendGift setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
        UIGraphicsEndImageContext();
        
        [cellWithButtons.buttonSendGift centerVertically];
        [cellWithButtons.buttonTakeGift centerVertically];
        
        return cellWithButtons;
    }
    return  [UITableViewCell new];
}


/*
 #pragma mark - Navigation
 
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)pressedButtonHeart:(id)sender
{
    if (self.gift.inWishes)
    {
        self.gift.inWishes = NO;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(40, 40), NO, scaleScreen);
        [WikiBonusStyleKit drawHeartButtonWithFrame:CGRectMake(0, 0, 40, 40) pressed:self.gift.inWishes];
        [self.buttonViewLikes setBackgroundImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal] ;
        UIGraphicsEndImageContext();
    }
    else if (!self.gift.inWishes)
    {
        self.gift.inWishes = YES;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(40, 40), NO, scaleScreen);
        [WikiBonusStyleKit drawHeartButtonWithFrame:CGRectMake(0, 0, 40, 40) pressed:self.gift.inWishes];
        [self.buttonViewLikes setBackgroundImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal] ;
        UIGraphicsEndImageContext();
    }
}

- (IBAction)buttonReviewsPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Функционал в разработке" message:nil
                      cancelButtonTitle:@"Закрыть"]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if (buttonIndex == 1)
         {
             // delete
         }
     }];
}

- (IBAction)buttonSendGiftPressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Функционал в разработке" message:nil
                      cancelButtonTitle:@"Закрыть"]
     showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
         if (buttonIndex == 1)
         {
             // delete
         }
     }];
}

- (IBAction)buttonTakeGiftPressed:(id)sender
{

}

#pragma mark CollectionView methods

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
        return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.gift.arrayFragments.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCellFragment *myCell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"fragmentCell" forIndexPath:indexPath];
    
    Fragmen *tmpFragment = [self.gift.arrayFragments objectAtIndex:indexPath.row];
    
    if (tmpFragment.isReceived)
    {
        myCell.imageViewFragment.image = tmpFragment.imageFragment;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.YYYY"];
         myCell.labelDateDead.text = [dateFormatter stringFromDate:tmpFragment.dateDead];
    }
    else
    {
        myCell.imageViewFragment.image = [UIImage imageNamed:@"noFragment.png"];
    }
    return myCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _currentFragment = [self.gift.arrayFragments objectAtIndex:indexPath.row];
    if (_currentFragment.isReceived)
    {
       [self performSegueWithIdentifier:@"showFragment" sender:self];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Не доступно" message:@"У вас нет этого фрагмента!" cancelButtonTitle:@"Закрыть"]
         showUsingBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
             if (buttonIndex == 1)
             {
                 // delete
             }
         }];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showFragment"])
    {
        FragmentViewController *fragmentVC = [segue destinationViewController];
        fragmentVC.currentFragment = _currentFragment;
        if (_currentFragment.isReceived)
        {
            [fragmentVC.navigationController popViewControllerAnimated:NO];
        }
    }
}

@end
