//
//  CategoryPointOfSale.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 11/25/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryPointOfSale : NSObject

@property (nonatomic, strong) NSString *nameCategory;
@property (nonatomic) NSInteger idCategory;
@property (nonatomic, strong) UIImage *imageCategory;

- (void)updatePointCategoryWithCategoryName:(NSString*)nameCategory idCategory:(NSInteger)idCategory imageСategory:(UIImage*)imageCategory;

@end
