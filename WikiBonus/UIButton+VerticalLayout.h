//
//  UIButton+VerticalLayout.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/25/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (VerticalLayout)

- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;

@end
