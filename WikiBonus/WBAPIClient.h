//
//  WBAPIClient.h
//  WikiBonus
//
//  Created by куборубо on 04.11.15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WBAPIClient : NSObject

#pragma mark Auth/Reg
+(void)registrationWithFirstName:(NSString*) firstName gender:(NSString*)genderUser email:(NSString*) email password:(NSString*) password
                        complete:(void (^)(id responseObject, NSError *error))complete;

+ (void)authLoginWithEmail:(NSString*)email
               andPassword:(NSString*)password
                  complete:(void (^)(id responseObject, NSError *error))complete;

+(void)socNetRegistrationWithUser:(User*)socNetUser
                         complete:(void (^)(id responseObject, NSError *error))complete;

+(void)changePasswordWithOldPassword:(NSString*)passwordOld newPassword:(NSString*)passwordNew complete:(void (^)(id responseObject, NSError *error))complete;

#pragma mark work with account
+(void)deleteAccauntWithComplete:(void (^)(id responseObject, NSError *error))complete;

+(void)exitFromAccountWithComplete:(void (^)(id responseObject, NSError *error))complete;

+(void)changeProfileWithParameters:(NSDictionary*)parameters complete:(void (^)(id responseObject, NSError *error))complete;

+(void)pointsOfSaleListWithParams:(NSDictionary*)parameters complete:(void (^)(id responseObject, NSError *error))complete;

+(void)geoSearch:(NSDictionary*)parameters complete:(void (^)(id responseObject, NSError *error))complete;

+ (void)companyInfoWithParams:(NSDictionary*)parameters complete:(void (^)(id responseObject, NSError *error))complete;

@end
