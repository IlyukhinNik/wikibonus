//
//  Fragmen.h
//  WikiBonus
//
//  Created by Nikita Ilyukhin on 12/18/15.
//  Copyright © 2015 CuboRubo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fragmen : NSObject

@property (nonatomic) NSInteger idFragment;
@property (nonatomic, strong) NSString *nameFragment;
@property (nonatomic, strong) NSString *descriptionFragment;
@property (nonatomic, strong) UIImage *imageFragment;
@property (nonatomic) NSDate *dateDead;
@property (nonatomic) BOOL isReceived;

-(void)updateFragmentWithID:(NSInteger)idFragment nameFragment:(NSString*)nameFragment imageFragment:(UIImage*)imageFragment dateDead:(NSDate*)dateDead isReceived:(BOOL)isRecieved;

@end
