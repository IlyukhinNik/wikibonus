//
//  User+CoreDataProperties.h
//  
//
//  Created by Nikita Ilyukhin on 10/30/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User: NSManagedObject

@property (nullable, nonatomic, retain) NSString *nameUser;
@property (nullable, nonatomic, retain) NSString *emailUser;
@property (nullable, nonatomic, retain) NSString *photoURL;
@property (nullable, nonatomic, retain) NSString *photoLocal;
@property (nullable, nonatomic, retain) NSString *accessTokenVK;
@property (nullable, nonatomic, retain) NSString *accessTokenFB;

@end


